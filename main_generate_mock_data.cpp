#include <sstream>
#include <time.h>
#include <chrono>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <string>
#include "Arsenal.h"
#include "Constants.h"
#include "Kernel.h"

int main(int argc, char **argv)
{
	kernel kern;
	kern.init("./data/CbarList_NNLO.dat");
	const int NX = 500;
	const int NP = 6;
	const int NZ = 31;

	int ialpha = 7;
	int ibeta  = 2;
	int is1 = 2;
	int is2 = 3;

	double alpha = -0.1*ialpha;
	double beta  = 1+0.1*ibeta;
	double s1 = 0.1*is1;
	double s2 = 0.1*is2;
	double res = 0;
	double * qx = new double[NX];
	double * dx = new double[NX];
	double * xx = new double[NX];
	for(int i=0; i<NX; i++){
		double x = exp(-0.1*(i+0.5));
		xx[i] = x;
		dx[i] = 0.1*x;
		double tmp = pow(x,alpha)*pow(1-x,beta)*(1+s1*sqrt(x)+s2*x);
		res += tmp*dx[i];
		qx[i] = tmp;
	}
	double norm = 0.5/res;
	cout << norm << endl;
	for(int i=0; i<NX; i++){qx[i] *= norm;}

	double P_list[NP] = {0.51, 0.76, 1.02, 1.27, 1.53, 1.78};
	double z0 = 0.001;	// GeV^{-1}
	double dz = 0.4;	// GeV^{-1}
	string suffix = "_alpha-0p"+to_string(ialpha)+"_beta-1p"+to_string(ibeta)+"_s1-0p"+to_string(is1)+"_s2-0p"+to_string(is2)+".dat";
	ofstream fout;
	fout.open("./data/pdf_mock"+suffix);
	fout << alpha <<"\t"<< beta <<"\t"<< s1 <<"\t"<< s2 <<"\t"<< norm << endl;
	for(int i=0; i<NX; i++){fout<< xx[i] <<"\t"<<dx[i] <<"\t"<<qx[i]<<endl;}
	fout.close();
	fout.open("./data/data_mock"+suffix);
	for (int iz=0; iz<NZ; iz++){
		double z = z0 + iz*dz;
		double Qreg = 0;
		for(int i=0; i<NX; i++){
			Qreg += 2*qx[i] * kern.C(xx[i]*z*P_reg, z) * dx[i];
		}
		fout << z <<"\t"<<Qreg;
		for(int ip=0; ip<NP; ip++){
			double p = P_list[ip];
			double Qp = 0;
			for(int i=0; i<NX; i++){
				Qp += 2*qx[i] * kern.C(xx[i]*z*p, z) * dx[i];
			}
			fout <<"\t"<<Qp/Qreg;
		}
		fout << endl;
	}
	fout.close();
	delete[] qx;
	delete[] xx;
	delete[] dx;
	kern.Free();
}
