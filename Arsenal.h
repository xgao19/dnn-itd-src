#include <chrono>
#include <string>
#include <random>
using namespace std;

#ifndef ARSENAL
#define ARSENAL

const string time_now() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    //strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
    strftime(buf, sizeof(buf), "[%X] ", &tstruct);
    return buf;
}

//	permutation_uniform selects a random permutation of N objects.
int* permutation_uniform(const int & n)
{
	int *p = new int[n];
	for (int i = 0; i < n; i++ ){
		p[i] = i;
	}
	for (int i = 0; i < n; i++ ){
		int j = (rand()%n);
		int k = p[i];
		p[i] = p[j];
		p[j] = k;
	}
	return p;
}

#endif