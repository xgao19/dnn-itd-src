#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <string.h>
#include <vector>
#include <random>
#include <cassert>
#include <omp.h>
#include <chrono>
#include "Multi_Dim_Normal_Distribution.h"
using namespace std;

#ifndef DNN__H
#define DNN__H

double elu(double x){
	if (x>0) return x;
	return exp(x) - 1;
}
double d_elu(double x){
	if (x>0) return 1;
	return exp(x);
}
double relu(double x){
	if(x>0) return x;
	return 0;
}
double d_relu(double x){
	if(x>0) return 1;
	return 0;
}
double leaky_relu(double x){
	if(x>0) return x;
	return 0.01*x;
}
double d_leaky_relu(double x){
	if(x>0) return 1;
	return 0.01;
}
double soft_plus(double x){
	return log(1+exp(x));
}
double d_soft_plus(double x){
	return 1/(1+exp(-x));
}

class DNN
{
public:
	DNN(){init_Q=0;}
	void Free();
	void init(int depth_in, vector<int> width_in, int N_pnt_in);
	void set_activation_function(vector<int> activation_function_in);
	vector<double> predict_prev(vector<double> x);
	vector<double> predict_next(vector<double> x);

	void optimizer_init();
	void set_optimizer_type(int optimizer_type_in);
	void update(double learning_rate, double regularizer);
	void update_momentum(double learning_rate, double regularizer);
	void update_adam(double learning_rate, double regularizer);
	void update_grad_desc(double learning_rate, double regularizer);

	void accept_update();
	void parameter_init();
	void assign_y_scale(double y0_in, double y1_in);
	void assign_x(double * x_in);
	double forward();
	double backward(int batch_size, int * x_in, double * dy_in);
	void import_model(int N_pnt_in, string filename);
	void import_weight_and_bias(string filename);
	void export_weight_and_bias(string filename);
	void summary();
    void parameter_save(); //To save the parameters for some purpose;
    void parameter_recover(); //To recover the saved parameters (Weight_save, Bias_save); 

	// l(layer) \in [1,...,depth]; i(element) \in [0,...,width[l]]; j(element) \in [0,...,width[l-1]]
	double W_prev(int l, int i, int j) {return Weight_prev[idx_W(l,i,j)];}
	double W_next(int l, int i, int j) {return Weight_next[idx_W(l,i,j)];}
	double b_prev(int l, int i)        {return Bias_prev[idx_b(l,i)];}
	double b_next(int l, int i)        {return Bias_next[idx_b(l,i)];}
	double d_W(int l, int i, int j)    {return d_Weight[idx_W(l,i,j)];}
	double d_b(int l, int i)           {return d_Bias[idx_b(l,i)];}
	double W_prev(int i) {return Weight_prev[i];}
	double W_next(int i) {return Weight_next[i];}
	double b_prev(int i) {return Bias_prev[i];}
	double b_next(int i) {return Bias_next[i];}
	double d_W(int i)    {return d_Weight[i];}
	double d_b(int i)    {return d_Bias[i];}
	int n_W() {return W_sep[depth];}
	int n_b() {return b_sep[depth];}
	void assign_W_prev(int l, int i, int j, double val) {Weight_prev[idx_W(l,i,j)] = val;}
	void assign_W_next(int l, int i, int j, double val) {Weight_next[idx_W(l,i,j)] = val;}
	void assign_b_prev(int l, int i, double val)        {  Bias_prev[idx_b(l,i)]   = val;}
	void assign_b_next(int l, int i, double val)        {  Bias_next[idx_b(l,i)]   = val;}
	void assign_W_prev(int i, double val) {Weight_prev[i] = val;}
	void assign_b_prev(int i, double val) {  Bias_prev[i] = val;}
	void assign_W_next(int i, double val) {Weight_next[i] = val;}
	void assign_b_next(int i, double val) {  Bias_next[i] = val;}
	// l(layer) \in [0,...,depth]; i(element) \in [0,...,width[l]]; k(element) \in [0,...,batch_size]
	// a[0] = inputs. u[0] and z[0] are meaningless, but keep them for simplicity.
	//double a_prev(int l, int i, int k) {return aFun_prev[idx_a(l,i,k)];}
	double a_next(int l, int i, int k) {return aFun_next[idx_a(l,i,k)];}
	//double u_prev(int l, int i, int k) {return uFun_prev[idx_a(l,i,k)];}
	double u_next(int l, int i, int k) {return uFun_next[idx_a(l,i,k)];}
	int get_depth() {return depth;}
	double get_y0() {return y_0;}
	double get_y1() {return y_1;}
	double get_width(int l) {return width[l];}
	void set_random_walk_CovMat(double* CovMat_in);
	void set_random_walk_InvCovMat(double* InvCovMat_in);
	void set_backup();
	double random_walk_step(double * delta);
private:
	int init_Q;
	int nx; // dimension of input
	int depth;
	int N_pnt;
	double y_0=0, y_1=1;
	int t_step = 1;
	int optimizer_type;

	int* width;
	int* activation_function_type;

    double * Weight_save;
    double * Bias_save;
	double * Weight_prev;
	double * Weight_next;
	double * d_Weight;
	double * m_Weight;
	double * s_Weight;
	double * Bias_prev;
	double * Bias_next;
	double * d_Bias;
	double * m_Bias;
	double * s_Bias;
	int * W_sep;
	int * b_sep;
	int * a_sep;
	double * aFun_prev;
	double * aFun_next;
	double * uFun_prev;
	double * uFun_next;
	double * zFun_temp;
	// l(layer) \in [1,...,depth]; i(element) \in [0,...,width[l]]; j(element) \in [0,...,width[l-1]]
	const int idx_W(int l, int i, int j) const {return W_sep[l-1]+i*width[l-1]+j;}
	const int idx_b(int l, int i)        const {return b_sep[l-1]+i;}
	// l(layer) \in [0,...,depth]; i(element) \in [0,...,width[l]]; k(element) \in [0,...,batch_size]
	// a[0] = inputs. u[0] and z[0] are meaningless, but keep them for simplicity.
	const int idx_a(int l, int i, int k) const {return (a_sep[l]+i)*N_pnt+k;}
	double z_temp(int l, int i, int k) {return zFun_temp[idx_a(l,i,k)];}
	//void assign_a_prev(int l, int i, int k, double val) { aFun_prev[idx_a(l,i,k)] = val; }
	void assign_a_next(int l, int i, int k, double val) { aFun_next[idx_a(l,i,k)] = val; }
	//void assign_u_prev(int l, int i, int k, double val) { uFun_prev[idx_a(l,i,k)] = val; }
	void assign_u_next(int l, int i, int k, double val) { uFun_next[idx_a(l,i,k)] = val; }
	void assign_z_temp(int l, int i, int k, double val) { zFun_temp[idx_a(l,i,k)] = val; }
	double sigma_fun(int l, double x);
	double sigma_drv(int l, double x);
	void test_index();

	multi_normal_distribution random_walk;
};

void DNN::init(int depth_in, vector<int> width_in, int N_pnt_in){
	assert(width_in.size()==depth_in+1);
	depth = depth_in;
	N_pnt = N_pnt_in;
	width = (int *) calloc(depth+1, sizeof(int));
	a_sep = (int *) calloc(depth+1, sizeof(int));
	b_sep = (int *) calloc(depth+1, sizeof(int));
	W_sep = (int *) calloc(depth+1, sizeof(int));
	activation_function_type = (int *) calloc(depth, sizeof(int));
	for(int i = 0; i<depth+1; i++){
		width[i] = width_in[i];
	}// elu	
	nx = width[0];
	for(int i = 0; i<depth; i++){
		activation_function_type[i]=2; //elu
	}// elu
	activation_function_type[depth-1] = 4;// tanh

	a_sep[0] = 0; b_sep[0] = 0; W_sep[0] = 0; 
	for(int i = 0; i<depth; i++){
		a_sep[i+1] = a_sep[i] + width[i]; 			// a_sep[l], starting point of l-layer 
		b_sep[i+1] = b_sep[i] + width[i+1];			// b_sep[l], starting point of (l+1)-layer 
		W_sep[i+1] = W_sep[i] + width[i]*width[i+1];// W_sep[l], starting point of (l+1)-layer 
	}
	int W_len = W_sep[depth];
	int b_len = b_sep[depth];
	int a_len = (b_len + nx) * N_pnt;
    Weight_save = (double *) calloc(W_len, sizeof(double));
    Bias_save = (double *) calloc(W_len, sizeof(double));
	Weight_prev = (double *) calloc(W_len, sizeof(double));
	Weight_next = (double *) calloc(W_len, sizeof(double));
	d_Weight    = (double *) calloc(W_len, sizeof(double));
	m_Weight    = (double *) calloc(W_len, sizeof(double));
	s_Weight    = (double *) calloc(W_len, sizeof(double));
	Bias_prev = (double *) calloc(b_len, sizeof(double));
	Bias_next = (double *) calloc(b_len, sizeof(double));
	d_Bias    = (double *) calloc(b_len, sizeof(double));
	m_Bias    = (double *) calloc(b_len, sizeof(double));
	s_Bias    = (double *) calloc(b_len, sizeof(double));
	aFun_prev = (double *) calloc(a_len, sizeof(double));
	aFun_next = (double *) calloc(a_len, sizeof(double));
	uFun_prev = (double *) calloc(a_len, sizeof(double));
	uFun_next = (double *) calloc(a_len, sizeof(double));
	zFun_temp = (double *) calloc(a_len, sizeof(double));
	init_Q = 1;
	optimizer_type = 0;
}

void DNN::Free(){
	free(width);
	free(a_sep);
	free(b_sep);
	free(W_sep);
    free(Weight_save);
    free(Bias_save);
	free(Weight_prev);
	free(Weight_next);
	free(d_Weight);
	free(m_Weight);
	free(s_Weight);
	free(Bias_prev);
	free(Bias_next);
	free(d_Bias);
	free(m_Bias);
	free(s_Bias);
	free(aFun_prev);
	free(aFun_next);
	free(uFun_prev);
	free(uFun_next);
	free(zFun_temp);
}
void DNN::test_index(){
	for(int l=0; l<=depth; l++){
		cout << l <<" "<< width[l] <<" "<< a_sep[l] <<" "<< b_sep[l] <<" "<< W_sep[l] <<endl;
		//<<" "<< b_size[l] <<" "<< W_size[l]<<endl;
	}
	cout << "Test a indexing:"<< endl;
	for(int l=0; l<=depth; l++){
		for(int i=0; i<width[l]; i++) {cout << idx_a(l,i,0) << " ";}
		cout << endl;
	}
	cout << "Test b indexing:"<< endl;
	for(int l=1; l<=depth; l++){
		for(int i=0; i<width[l]; i++) {cout << idx_b(l,i) << " ";}
		cout << endl;
	}
	cout << "Test W indexing:"<< endl;
	for(int l=1; l<=depth; l++){
		for(int i=0; i<width[l]; i++){
			for(int j=0; j<width[l-1]; j++){cout << idx_W(l,i,j) << " ";}
		}
		cout << endl;
	}
}
double DNN::sigma_fun(int l, double x){
	if (activation_function_type[l-1]==0) return tanh(x); //tanh
	if (activation_function_type[l-1]==1) return relu(x); //relu
	if (activation_function_type[l-1]==2) return elu(x);  //elu
	if (activation_function_type[l-1]==3) return leaky_relu(x);  //leaky_relu
	if (activation_function_type[l-1]==4) return soft_plus(x);  //soft_plus
	return x;
}
double DNN::sigma_drv(int l, double x){
	if (activation_function_type[l-1]==0) return 1./cosh(x)/cosh(x); //tanh
	if (activation_function_type[l-1]==1) return d_relu(x); //relu
	if (activation_function_type[l-1]==2) return d_elu(x);  //elu
	if (activation_function_type[l-1]==3) return d_leaky_relu(x);  //leaky_relu
	if (activation_function_type[l-1]==4) return d_soft_plus(x);  //soft_plus
	return 1;
}
void DNN::set_activation_function(vector<int> activation_function_in){
	assert(activation_function_in.size()==depth);
	for (int i=0; i<depth; i++){
		activation_function_type[i] = activation_function_in[i];
	}
	return;
}
void DNN::assign_y_scale(double y0_in, double y1_in){
	y_0 = y0_in;
	y_1 = y1_in;
}
void DNN::assign_x(double * x_in){
	#pragma omp parallel for collapse(2) schedule(guided)
	for (int k=0; k<N_pnt; k++)
	for (int i=0; i<nx; i++){
		//assign_a_prev(0, i, k, x_in[k*nx+i]);
		assign_a_next(0, i, k, x_in[k*nx+i]);
	}
}
void DNN::parameter_init(){
	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	default_random_engine rnd_gen(seed);
	normal_distribution<double> gauss(0.0,1.0);
	for(int l=1; l<= depth; l++){
		double w = sqrt(2./width[l-1]);
		#pragma omp parallel for
		for(int i=0; i<width[l]; i++)
		{
			//Bias_prev[b_sep[l-1]+i] = norm_dist::gauss(norm_dist::rnd_gen) * w;
			Bias_prev[b_sep[l-1]+i] = gauss(rnd_gen) * w;
			for(int j=0; j<width[l-1]; j++){
				//Weight_prev[W_sep[l-1]+i*width[l-1]+j] = norm_dist::gauss(norm_dist::rnd_gen) * w;
				Weight_prev[W_sep[l-1]+i*width[l-1]+j] = gauss(rnd_gen) * w;
			}
		}
	}
	optimizer_init();
	return;
}
void DNN::optimizer_init(){
	#pragma omp parallel for
	for(int i=0; i<W_sep[depth]; i++){
		m_Weight[i] = 0;
		s_Weight[i] = 0;
	}
	#pragma omp parallel for
	for(int i=0; i<b_sep[depth]; i++){ 
		m_Bias[i] = 0;
		s_Bias[i] = 0; 
	}
	t_step = 1;
	return;
}
double DNN::forward(){ // compute the functions
	auto time_0 = chrono::system_clock::now();
	for(int l=1; l<= depth; l++){
		#pragma omp parallel for collapse(2) schedule(guided)
		for(int k=0; k<N_pnt; k++)
		for(int i=0; i<width[l]; i++)
		{
			double val_z = b_prev(l,i);
			for(int j=0; j<width[l-1]; j++)
			{
				val_z += W_prev(l,i,j)*a_next(l-1,j,k);
			}
			double val_a = sigma_fun(l,val_z);
			double val_u = sigma_drv(l,val_z);
			assign_z_temp(l,i,k,val_z);
			assign_a_next(l,i,k,val_a);
			assign_u_next(l,i,k,val_u);
		}
	}
	auto time_1 = chrono::system_clock::now();
	chrono::duration<double> time_diff = time_1 - time_0;
	return time_diff.count();
}
double DNN::backward(int batch_size, int * x_in, double * dy_in){ // compute the derivatives
	auto time_0 = chrono::system_clock::now();
	int max_width=0;
	for(int l=0; l<=depth; l++){
		if (max_width<width[l]) {max_width = width[l];}
	}
	int N_batch = batch_size;
	double * da_list = (double *) calloc(max_width*N_batch, sizeof(double));
	#pragma omp parallel for
	for(int ii=0; ii<max_width*N_batch; ii++){da_list[ii] = 1;}
	for(int l=depth; l>0; l--){
		double * db_list = (double *) calloc(width[l]*N_batch, sizeof(double));
		double * dW_list = (double *) calloc(width[l]*width[l-1]*N_batch, sizeof(double));
		#pragma omp parallel for collapse(2) schedule(guided)
		for(int i=0; i<width[l]; i++)
		for(int k=0; k<N_batch; k++){
			db_list[i*N_batch + k] = da_list[i*N_batch + k] * u_next(l,i,x_in[k]);
			for(int j=0; j<width[l-1]; j++)
			{
				dW_list[(i*width[l-1]+j)*N_batch + k] 
					= db_list[i*N_batch + k] * a_next(l-1,j,x_in[k]);
			}
		}
		#pragma omp parallel for
		for(int ii=0; ii<max_width*N_batch; ii++){da_list[ii] = 0;}
		#pragma omp parallel for collapse(2) schedule(guided)
		for(int i=0; i<width[l-1]; i++)
		for(int k=0; k<N_batch; k++){
			double val_a = 0;
			for(int j=0; j<width[l]; j++){
				val_a += W_prev(l,j,i)*db_list[j*N_batch + k];
			}
			da_list[i*N_batch + k] = val_a;
		}
		// sum-up over input points and update the global value
		for(int i=0; i<width[l]; i++)
		{
			double val_b = 0;
			#pragma omp parallel for default(shared) reduction(+: val_b)
			for(int k=0; k<N_batch; k++){
				val_b += db_list[i*N_batch + k] * dy_in[k];
			}
			d_Bias[b_sep[l-1]+i] = val_b;
		}
		for(int i=0;i<width[l];i++) for(int j=0;j<width[l-1];j++)
		{
			double val_W = 0;
			#pragma omp parallel for default(shared) reduction(+: val_W)
			for(int k=0; k<N_batch; k++){
				val_W += dW_list[(i*width[l-1]+j)*N_batch + k] * dy_in[k];
			}
			d_Weight[W_sep[l-1]+i*width[l-1]+j] = val_W;
		}
		free(db_list);
		free(dW_list);
	}
	free(da_list);
	auto time_1 = chrono::system_clock::now();
	chrono::duration<double> time_diff = time_1 - time_0;
	return time_diff.count();
}
void DNN::set_optimizer_type(int optimizer_type_in){
	optimizer_type = optimizer_type_in;
}
void DNN::update(double learning_rate, double regularizer){
	if (optimizer_type==0) {return update_adam     (learning_rate, regularizer);}
	if (optimizer_type==1) {return update_momentum (learning_rate, regularizer);}
	if (optimizer_type==2) {return update_grad_desc(learning_rate, regularizer);}
}
void DNN::update_grad_desc(double learning_rate, double regularizer){
	#pragma omp parallel for
	for(int i=0; i<W_sep[depth]; i++){ 
		Weight_next[i] = 
			(1-regularizer*learning_rate) * Weight_prev[i] 
			- learning_rate * d_Weight[i]; 
	}
	#pragma omp parallel for
	for(int i=0; i<b_sep[depth]; i++){ 
		Bias_next[i] =
			(1-regularizer*learning_rate) * Bias_prev[i]
			- learning_rate * d_Bias[i];
	}
	return;
}
void DNN::update_adam(double learning_rate, double regularizer){
	double beta_m=0.9, beta_s=0.99;
	double beta_mg = 1-beta_m;
	double beta_sg = 1-beta_s;
	double fac_m = 1-pow(beta_m,t_step);
	double fac_s = 1-pow(beta_s,t_step);
	double epsilon=1E-4;
	#pragma omp parallel for
	for(int i=0; i<W_sep[depth]; i++){
		double g = d_Weight[i] + regularizer * Weight_prev[i];
		m_Weight[i] = beta_m * m_Weight[i] + beta_mg * g;
		s_Weight[i] = beta_s * s_Weight[i] + beta_sg * g*g;
		double g_hat = (m_Weight[i]/fac_m) / (sqrt(s_Weight[i]/fac_s + epsilon));
		Weight_next[i] = Weight_prev[i] 
			- learning_rate * g_hat; 
	}
	#pragma omp parallel for
	for(int i=0; i<b_sep[depth]; i++){ 
		double g = d_Bias[i] + regularizer * Bias_prev[i];
		m_Bias[i] = beta_m * m_Bias[i] + beta_mg * g;
		s_Bias[i] = beta_s * s_Bias[i] + beta_sg * g*g;
		double g_hat = (m_Bias[i]/fac_m) / (sqrt(s_Bias[i]/fac_s + epsilon));
		Bias_next[i] = Bias_prev[i]
			- learning_rate * g_hat; 
	}
	t_step += 1;
	return;
}
void DNN::update_momentum(double learning_rate, double regularizer){
	double beta = 0.8;
	#pragma omp parallel for
	for(int i=0; i<W_sep[depth]; i++){
		double g = d_Weight[i] + regularizer * Weight_prev[i];
		m_Weight[i] = beta * m_Weight[i] - g;
		Weight_next[i] = Weight_prev[i] + learning_rate * m_Weight[i]; 
	}
	#pragma omp parallel for
	for(int i=0; i<b_sep[depth]; i++){
		double g = d_Bias[i] + regularizer * Bias_prev[i];
		m_Bias[i] = beta * m_Bias[i] - g;
		Bias_next[i] = Bias_prev[i] + learning_rate * m_Bias[i]; 
	}
	t_step += 1;
	return;
}
void DNN::accept_update(){
	#pragma omp parallel for
	for(int i=0; i<W_sep[depth]; i++){ Weight_prev[i] = Weight_next[i]; }
	#pragma omp parallel for
	for(int i=0; i<b_sep[depth]; i++){ Bias_prev[i] = Bias_next[i]; }
	#pragma omp parallel for
	for(int i=0; i<(b_sep[depth]+nx)*N_pnt; i++){
		aFun_prev[i] = aFun_next[i];
		uFun_prev[i] = uFun_next[i];
	}
}
void DNN::parameter_save(){
    //#pragma omp parallel for
    for(int i=0; i<W_sep[depth]; i++){ Weight_save[i] = Weight_prev[i]; }
    //#pragma omp parallel for
    for(int i=0; i<b_sep[depth]; i++){ Bias_save[i] = Bias_prev[i]; }
}
void DNN::parameter_recover(){
    //#pragma omp parallel for
    for(int i=0; i<W_sep[depth]; i++){ Weight_prev[i] = Weight_save[i]; }
    //#pragma omp parallel for
    for(int i=0; i<b_sep[depth]; i++){ Bias_prev[i] = Bias_save[i]; }
}
vector<double> DNN::predict_next(vector<double> x){// predict value using _next
	assert(x.size()==nx);
	double * a_temp = (double *) calloc((b_sep[depth] + nx), sizeof(double));
	for(int i=0; i<nx; i++){ a_temp[i] = x[i]; }
	for(int l=1; l<= depth; l++){
		for(int i=0; i<width[l]; i++)
		{
			double val_z = b_next(l,i);
			for(int j=0; j<width[l-1]; j++)
			{
				val_z += W_next(l,i,j)*a_temp[a_sep[l-1]+j];
			}
			a_temp[a_sep[l]+i] = sigma_fun(l, val_z);
		}
	}
	vector<double> y(width[depth]);
	for(int i=0; i<width[depth]; i++){y[i]=(a_temp[a_sep[depth]+i] + y_0)*y_1;}
	free(a_temp);
	return y;
}
vector<double> DNN::predict_prev(vector<double> x){// predict value using _prev
	assert(x.size()==nx);
	double * a_temp = (double *) calloc((b_sep[depth] + nx), sizeof(double));
	for(int i=0; i<nx; i++){ a_temp[i] = x[i]; }
	for(int l=1; l<= depth; l++){
		for(int i=0; i<width[l]; i++)
		{
			double val_z = b_prev(l,i);
			for(int j=0; j<width[l-1]; j++)
			{
				val_z += W_prev(l,i,j)*a_temp[a_sep[l-1]+j];
			}
			a_temp[a_sep[l]+i] = sigma_fun(l, val_z);
		}
	}
	vector<double> y(width[depth]);
	for(int i=0; i<width[depth]; i++){y[i]=(a_temp[a_sep[depth]+i] + y_0)*y_1;}
	free(a_temp);
	return y;
}
void DNN::summary(){
	cout << "------------------------------------------------------------"<<endl;
	cout << "Depth : " << depth <<endl;
	cout << " y_0  : " << y_0 <<endl;
	cout << " y_1  : " << y_1 <<endl;
	cout << " n_x  : " << width[0] << endl;
	for (int l = 0; l < depth; l++)
	{
		cout << "At "<<l<<"-th layer, width = "<< width[l+1]
		<< ", activation_function_type = "<< activation_function_type[l]<<endl;
	}
	cout << "------------------------------------------------------------"<<endl;
	return;
}
void DNN::export_weight_and_bias(string filename="./model_weight_and_bias.dat"){
	ofstream fout;
	fout.open(filename);
	fout << depth << "\t=depth"<<endl;
	fout << y_0 << "\t=y_0"<<endl;
	fout << y_1 << "\t=y_1"<<endl;
	for(int l=0; l<=depth; l++){ fout<<width[l]<<"\t";}
	fout <<"=width\t(w_x,w_1,...,w_y)"<<endl;
	for(int l=0; l<depth; l++){ fout<<activation_function_type[l]<<"\t";}
	fout <<"=activation_function_type\t(s_1,...,s_y)"<<endl;
	for(int l=1; l<=depth; l++){
		fout <<l<<"\tth-layer"<<endl;
		fout <<width[l]<<"\t"<<width[l-1]<<"\t=dim(W_l)"<<endl;
		for(int i=0; i<width[l]; i++){
			for(int j=0; j<width[l-1]; j++){
				fout << W_prev(l,i,j) <<"\t";
			}
			fout << endl;
		}
		fout <<width[l]<<"\t=len(b_l)"<<endl;
		for(int i=0; i<width[l]; i++){
			fout << b_prev(l,i) <<"\t";
		}
		fout << endl;
	}
	fout << "End_of_File" << endl;
	fout.close();
	return;
}
void DNN::import_weight_and_bias(string filename="./model_weight_and_bias.dat"){
	ifstream fin;
	int buffer_int;
	double buffer_dbl;
	string buffer_str;
	fin.open(filename);
	fin >> buffer_int >> buffer_str;	assert(buffer_int==depth); assert(buffer_str.compare("=depth")==0);
	fin >> buffer_dbl >> buffer_str;	assert(buffer_str.compare("=y_0")==0); y_0 = buffer_dbl;
	fin >> buffer_dbl >> buffer_str;	assert(buffer_str.compare("=y_1")==0); y_1 = buffer_dbl;
	for(int l=0; l<=depth; l++){ fin >> buffer_int; assert(buffer_int==width[l]); }
		fin >> buffer_str; 				assert(buffer_str.compare("=width")==0);
		fin >> buffer_str; 				assert(buffer_str.compare("(w_x,w_1,...,w_y)")==0);
	for(int l=0; l<depth; l++){ fin >> buffer_int; activation_function_type[l]=buffer_int; }
		fin >> buffer_str; 				assert(buffer_str.compare("=activation_function_type")==0);
		fin >> buffer_str; 				assert(buffer_str.compare("(s_1,...,s_y)")==0);
	// Read weight and bias for different layers
	for(int l=1; l<=depth; l++){
		// layer head
		fin >> buffer_int;				assert(buffer_int==l);
		fin >> buffer_str; 				assert(buffer_str.compare("th-layer")==0);
		// weight
		fin >> buffer_int;				assert(buffer_int==width[l]);
		fin >> buffer_int;				assert(buffer_int==width[l-1]);
		fin >> buffer_str; 				assert(buffer_str.compare("=dim(W_l)")==0);
		for(int i=0; i<width[l]; i++){
			for(int j=0; j<width[l-1]; j++){
				fin >> buffer_dbl;
				assign_W_prev(l,i,j,buffer_dbl);
			}
		}
		// bias
		fin >> buffer_int;				assert(buffer_int==width[l]);
		fin >> buffer_str; 				assert(buffer_str.compare("=len(b_l)")==0);
		for(int i=0; i<width[l]; i++){
			fin >> buffer_dbl;
			assign_b_prev(l,i,buffer_dbl);
		}
	}
	fin >> buffer_str; 					assert(buffer_str.compare("End_of_File")==0);
	fin.close();
	return;
}
void DNN::import_model(int N_pnt_in, string filename="./model_weight_and_bias.dat"){
	assert(init_Q==0);
	ifstream fin;
	int buffer_int;
	double buffer_dbl;
	string buffer_str;
	fin.open(filename);
	fin >> depth >> buffer_str;			assert(buffer_str.compare("=depth")==0);
	fin >> buffer_dbl >> buffer_str;	assert(buffer_str.compare("=y_0")==0); y_0 = buffer_dbl;
	fin >> buffer_dbl >> buffer_str;	assert(buffer_str.compare("=y_1")==0); y_1 = buffer_dbl;
	vector<int> width_in(depth+1);
	for(int l=0; l<=depth; l++){ fin >> buffer_int; width_in[l]=buffer_int; }
		fin >> buffer_str; 				assert(buffer_str.compare("=width")==0);
		fin >> buffer_str; 				assert(buffer_str.compare("(w_x,w_1,...,w_y)")==0);
	init(depth, width_in, N_pnt_in);
	for(int l=0; l<depth; l++){ fin >> buffer_int; activation_function_type[l]=buffer_int; }
		fin >> buffer_str; 				assert(buffer_str.compare("=activation_function_type")==0);
		fin >> buffer_str; 				assert(buffer_str.compare("(s_1,...,s_y)")==0);
	// Read weight and bias for different layers
	for(int l=1; l<=depth; l++){
		// layer head
		fin >> buffer_int;				assert(buffer_int==l);
		fin >> buffer_str; 				assert(buffer_str.compare("th-layer")==0);
		// weight
		fin >> buffer_int;				assert(buffer_int==width[l]);
		fin >> buffer_int;				assert(buffer_int==width[l-1]);
		fin >> buffer_str; 				assert(buffer_str.compare("=dim(W_l)")==0);
		for(int i=0; i<width[l]; i++){
			for(int j=0; j<width[l-1]; j++){
				fin >> buffer_dbl;
				assign_W_prev(l,i,j,buffer_dbl);
			}
		}
		// bias
		fin >> buffer_int;				assert(buffer_int==width[l]);
		fin >> buffer_str; 				assert(buffer_str.compare("=len(b_l)")==0);
		for(int i=0; i<width[l]; i++){
			fin >> buffer_dbl;
			assign_b_prev(l,i,buffer_dbl);
		}
	}
	fin >> buffer_str; 					assert(buffer_str.compare("End_of_File")==0);
	fin.close();
	return;
}
void DNN::set_random_walk_CovMat(double* CovMat_in){
	random_walk.init(W_sep[depth]+b_sep[depth]);
	random_walk.set_covmat(CovMat_in);
}
void DNN::set_random_walk_InvCovMat(double* InvCovMat_in){
	random_walk.init(W_sep[depth]+b_sep[depth]);
	random_walk.set_invcovmat(InvCovMat_in);
}
void DNN::set_backup(){//Use "_next" as back-up point;
	#pragma omp parallel for
	for(int i=0; i<W_sep[depth]; i++){ Weight_next[i] = Weight_prev[i]; }
	#pragma omp parallel for
	for(int i=0; i<b_sep[depth]; i++){ Bias_next[i] = Bias_prev[i]; }
}
double DNN::random_walk_step(double * delta){
	double result = random_walk.get_sample(delta);
	#pragma omp parallel for
	for(int i=0; i<W_sep[depth]; i++){ Weight_prev[i] = Weight_next[i] + delta[i]; }
	#pragma omp parallel for
	for(int i=0; i<b_sep[depth]; i++){   Bias_prev[i] = Bias_next[i] + delta[i+W_sep[depth]]; }
	forward();
	return result;
}

#endif
