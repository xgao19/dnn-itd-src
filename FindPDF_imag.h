#include <sstream>
#include <time.h>
#include <chrono>
#include <iomanip>
#include "Arsenal.h"
#include "DNN.h"
#include "Constants.h"
#include "Kernel_imag.h"

#ifndef FINDPDF
#define FINDPDF

class findpdf{
public:
	findpdf(){};
	void init();
	void Free();
    void load_lattice_data(const string& directory, const string& prefix, const double& delta_in,
     const int& sample, const int& Ns_in, const int& P0_in, const int& NP_in, const int* Plist_in,
     const int& Zmin_in, const int& Zmax_in, const double& dzfm_in);
	void model_fit(int sample, int attemp, double learning_rate_start, const string& prior_dir);
private:
	kernel kern;

	DNN q_DNN;
	int depth_DNN;

	void update_qx();
    void update_Fx();
	double compute_loss();
    void export_moments(const string& filename);
	void export_pdf(const string& filename);
    void export_Q(const string& filenameQ);
	void export_parameters(const string& filename);
    void export_ITD(const string& filenameITD);
	void import_parameters(const string& filename);

    int P0;    // reference momentum
	const int* Plist; // list of Pz
	int NP; // number of Pz
	int Zmin; // start point of z
    int Zmax; // end point of z	
	int NZ; //number of z
	int N_data;
    int N_step = 20;
    int i_step;
    int i_epoch;

	double dz;// = 0.04/hbarc;	// GeV^{-1} // need input
	double z0;// = dz;			// GeV^{-1}
	double dP;// = 2*M_PI/(Ns*dz); // GeV

	double zi(const int& iz)const {return iz*dz;}
	double *  P_obs;
	int    *  z_obs; // index of z, rather than z.
	double *  Q_obs;
	double ** Qcov_obs;
    double delta; // limit the contribtuion of the DNN part

    double prior_beta = 0;
    double priorWidth_beta = 1E+10;

	const int NX = 1000;
	double norm;
	double alpha;
	double beta;
	double r;
	double d_alpha;
	double d_beta;
	double d_r;
    double d_norm;
	double m_alpha;
	double m_beta;
	double m_r;
    double m_norm;
	double s_alpha;
	double s_beta;
	double s_r;
    double s_norm;
	//update alpha and beta according to ADAM
	double beta_m=0.9;
	double beta_s=0.99;
	double beta_mg = 1-beta_m;
	double beta_sg = 1-beta_s;
	double epsilon=1E-4;
	double fac_m;
	double fac_s;

	double * qx;
    double * Fx;  //derivative of qx
	double * dq;
	double *  x;
	double * dx;
	double chi_sq;
	stringstream ss_out;
    stringstream ss_save;
    stringstream ss_ITD;
};

void findpdf::init(){
	time_t my_time = time(NULL);
	cout << time_now() <<"Program started." << endl;
	kern.init("./data/CbarList_NNLO_imag.dat");
	alpha = 0;
	beta  = 1;
	norm  = 1;
	qx = (double *) calloc (NX, sizeof(double));
    Fx = (double *) calloc (NX, sizeof(double));
	dq = (double *) calloc (NX, sizeof(double));
	x  = (double *) calloc (NX, sizeof(double));
	dx = (double *) calloc (NX, sizeof(double));
	for (int i=0; i<NX; i++){
		double tmp = exp(-0.01*(i+0.5));
		 x[i] = tmp;
		dx[i] = 0.01*tmp;
	}
}

void findpdf::Free(){
	kern.Free();
	q_DNN.Free();
	free(dq);
	free(qx);
    free(Fx);
	free(dx);
	free(x);
	free(P_obs);
	free(z_obs);
	free(Q_obs);
	free(Qcov_obs);
}

void findpdf::load_lattice_data(const string& directory, const string& prefix, const double& delta_in,
    const int& sample, const int& Ns_in, const int& P0_in, const int& NP_in, const int* Plist_in,
    const int& Zmin_in,	const int& Zmax_in, const double& dzfm_in){
	int Ns = Ns_in;	 // number of grids in space coordinate
    delta = delta_in;
    NP = NP_in;
    P0 = P0_in;
    Plist = Plist_in;
	Zmin = Zmin_in;
	Zmax = Zmax_in;
	NZ = Zmax - Zmin + 1;  // number of points in z 
	dz = dzfm_in/hbarc;	 // GeV^{-1}
	z0 = dz;			 // GeV^{-1}
	dP = 2*M_PI/(Ns*dz); // GeV
	N_data = NP*NZ;

	P_obs  = (double *) calloc (N_data, sizeof(double));
	z_obs  = (int	*) calloc (N_data, sizeof(int   ));
	Q_obs  = (double *) calloc (N_data, sizeof(double));
	Qcov_obs = (double **) calloc (N_data, sizeof(double*));

	string surfix, mid;
	if (sample == -1){ // for mean
		mid = "/";
		surfix = ".Mean.X1-32";
	}
	else{ //each sample
		mid = "/samples/";
		surfix = ".X1-32.sample"+to_string(sample);
	}
	double Qp;
	int pi, zi, PN;
	ifstream fin;
    string filename_val = directory+mid+prefix+surfix+".imag";
    cout << "Reading " << filename_val << endl;
    fin.open(filename_val);
    for(int ip=0; ip<NP; ip++) {
        PN = Plist[ip];
        for (int iz=1; iz<Zmin; iz++){
            fin >> pi >> zi >> Qp;
        }
        for (int iz=0; iz<NZ; iz++){
            fin >> pi >> zi >> Qp;
            //cout << pi << "==" << PN << ", " << zi << "==" << iz+Zmin << endl;
            assert(pi==(PN));
            assert(zi==(iz+Zmin));
            P_obs[ip*NZ+iz] = (PN)*dP;
            z_obs[ip*NZ+iz] = iz+Zmin;
            Q_obs[ip*NZ+iz] = Qp;
        }
        for (int iz=NZ; iz<32-Zmin+1; iz++){
            fin >> pi >> zi >> Qp;
            //cout << pi << "==" << PN << ", " << zi << "==" << iz+Zmin << endl;
        }
    }
    fin.close();

    string filename_unc = directory+"/"+prefix+".invCov.X1-32"+".imag";
    cout << "Reading " << filename_unc << endl;
    fin.open(filename_unc);
    double tmp;
    for(int ip=0; ip<NP; ip++) {
        PN = Plist[ip];
        for (int iz=1; iz<Zmin; iz++){
            for (int ii=0; ii<32*NP; ii++) {
                fin >> tmp;
            }
        }
        for (int iz=0; iz<NZ; iz++){
            Qcov_obs[ip*NZ+iz] = (double *) calloc (N_data, sizeof(double));
            for (int ipp=0; ipp<NP; ipp++) {
                for (int izz=1; izz<Zmin; izz++){
                    fin >> tmp;
                }
                for (int izz=0; izz<NZ; izz++){
                    fin >> tmp;
                    Qcov_obs[ip*NZ+iz][ipp*NZ+izz] = tmp;
                    //cout << Qcov_obs[ip*NZ+iz][ipp*NZ+izz] << " ";
                }
                for (int izz=NZ; izz<32-Zmin+1; izz++){
                    fin >> tmp;
                }
            }
            //cout << endl;
        }
        for (int iz=NZ; iz<32-Zmin+1; iz++){
            for (int ii=0; ii<32*NP; ii++) {
                fin >> tmp;
            }
        }
    }
    fin.close();
    cout << "Read  Z in ["<<Zmin<<","<<Zmax<<"], NZ = "<<NZ<<endl;
    cout << "Read Pz in {";
    for(int ip=0; ip<NP; ip++) {
        cout<<","<<Plist[ip];
    }
    cout<<"}, NP = "<<NP<<endl;
    cout << "Read&accept "<<N_data<<" data points." <<endl;
}

void findpdf::update_qx(){
	double res = 0;
	for(int i=0; i<NX; i++){
        double q_loc = norm * pow(x[i],alpha) * pow(1-x[i],beta) * (1 + delta*sin(q_DNN.a_next(depth_DNN,0,i)));
		qx[i] = q_loc;
	}
}

void findpdf::update_Fx(){
    for(int i=0; i<NX; i++){
        double F_loc = norm * pow(x[i],alpha) * pow(1-x[i],beta) * delta*cos(q_DNN.a_next(depth_DNN,0,i));
        Fx[i] = F_loc; //norm come from update_qx()
    }
}

void findpdf::export_moments(const string& filename){
    update_qx();
    ofstream fout(filename);
    //fout << "#x\tq(x)"<<endl;
    for (int n=0; n<=50; n++){
        double moms = 0.;
        for (int i=0; i<NX; i++){
            moms += qx[i] * pow(x[i],n)  *2*dx[i];
        }
        fout << n <<"\t"<<moms<<endl;
    }
    fout.close();

    }

void findpdf::export_pdf(const string& filename){
	update_qx();
	ofstream fout(filename);
	//fout << "#x\tq(x)"<<endl;
	for (int i=0; i<NX; i++){
		fout << x[i]<<"\t"<<2*qx[i]<<endl;
	}
	fout.close();
}

void findpdf::export_Q(const string& filenameQ){
    update_qx();
    double z_step = 0.01/hbarc;
    double z_max  = 501;
    double * Q_reg  = (double *) calloc (500, sizeof(double));
    //prepara regularization point
	for (int iz = 1; iz<z_max; iz++){
        double z = iz * z_step;
		double res = r*P0*P0*dz*dz;
		for (int i=0; i<NX; i++){
			res += qx[i] * kern.C(x[i]*z*P0, z) *2*dx[i];
		}
		//cout << "P0 = "<< P0 << ", iz = " << iz << ", res = " << res << ", r*P0*P0*dz*dz = " << r*P0*P0*dz*dz <<endl;
		Q_reg[iz-1] = res;
        Q_reg[iz-1] = 1;
	}
    for (int ip=0; ip<NP; ip++){
        int pi = Plist[ip];
        double P = pi*dP;
       
        string filename_ip = filenameQ+"-Q-PN"+to_string(pi)+".dat";
        ofstream fout(filename_ip);
        fout << pi <<"\t" << P <<"\t"<< 0 << "\t" << 0 <<"\t"<< 0 <<endl;

        for (int iz = 1; iz<z_max; iz++){
            double z = iz * z_step;
            double res = r*P*P*dz*dz;
            for (int i=0; i<NX; i++){
                res += qx[i] * kern.C(x[i]*z*P, z) *2*dx[i];
            }
        double Q_bar = res/Q_reg[iz-1];
        fout << pi <<"\t" << P <<"\t"<< z*hbarc << "\t" << z*P <<"\t"<< Q_bar <<endl;

        }
        fout.close(); 
    }
}

void findpdf::export_ITD(const string& filenameITD){
    update_qx();
    ofstream Qsave(filenameITD);
    for (int iL=0; iL<2000; iL++){
        double ITD = 0;
        for (int i=0; i<NX; i++){
            ITD += qx[i] * sin(x[i]*iL/100.0) *2*dx[i];
        }
        Qsave << iL/100.0 << "\t" << ITD << endl;
    }
    Qsave.close();
}

double findpdf::compute_loss(){
	auto time_0 = chrono::system_clock::now();
	chi_sq = 0;

	for(int i=0; i<NX; i++){dq[i] = 0;}
	update_qx();
    update_Fx();

	double * Q_diff = (double *) calloc (N_data, sizeof(double));
	double * Q_reg  = (double *) calloc (NZ, sizeof(double));
    double * Q_bar  = (double *) calloc (N_data, sizeof(double));
	double * DxList = (double *) calloc (NX, sizeof(double));

	//prepare regularization point
	for (int iz = Zmin; iz<Zmax+1; iz++){
		double z = zi(iz);
		double res = r*P0*P0*dz*dz;
		for (int i=0; i<NX; i++){
			res += qx[i] * kern.C(x[i]*z*P0, z) *2*dx[i];
		}
		//cout << "P0 = "<< P0 << ", iz = " << iz << ", res = " << res << ", r*P0*P0*dz*dz = " << r*P0*P0*dz*dz <<endl;
		Q_reg[iz-Zmin] = res;
        Q_reg[iz-Zmin] = 1;
	}
    // prepare rITD
    for (int id=0; id<N_data; id++){
        int iz = z_obs[id];
        double z = zi(iz);
        double P = P_obs[id];
        double Q = Q_obs[id];
        double Q_loc = r*P*P*dz*dz;
        for (int i=0; i<NX; i++){
            Q_loc += qx[i] * kern.C(x[i]*z*P, z) *2*dx[i];
        }
        Q_bar[id] = Q_loc/Q_reg[iz-Zmin];
    }

    // compute loss
    for (int i=0; i<NX; i++){DxList[i]=0;}
    d_r = 0;
    d_norm = 0;
    for (int id=0; id<N_data; id++){
        int iz = z_obs[id];
        double z_i = zi(iz);
        double P_i = P_obs[id];
        double Q_i = Q_obs[id];
        for (int jd=id; jd<id+1; jd++){
            //cout << id << "<>" << jd << ", cov_ij = " << Qcov_obs[id][jd] << endl;
            int jz = z_obs[jd];
            double z_j = zi(jz);
            double P_j = P_obs[jd];
            double Q_j = Q_obs[jd];
            chi_sq += Qcov_obs[id][jd]*(Q_bar[id] - Q_i)*(Q_bar[jd] - Q_j);
            d_r += Qcov_obs[id][jd]*dz*dz*(P_j*P_j-P0*P0*Q_bar[jd])*((Q_bar[id] - Q_i)/Q_reg[jz-Zmin]);
            d_norm += 2*Qcov_obs[id][jd]*(Q_bar[id] - Q_i)*Q_bar[jd]/norm;
            for (int i=0; i<NX; i++){
                DxList[i] += Qcov_obs[id][jd]*(Q_bar[id] - Q_i)/Q_reg[jz-Zmin] * (kern.C(x[i]*P_j*z_j,z_j) - Q_bar[jd]*kern.C(x[i]*P0*z_j,z_j));
            }
            //cout << (Q_bar[id] - Q_i) << " " << (Q_bar[jd] - Q_j) << " " << kern.C(x[10]*P0*z_j,z_j) << endl;
        }
    }

	d_alpha = 0;
	d_beta  = (beta-prior_beta)/priorWidth_beta/priorWidth_beta;
	for (int i=0; i<NX; i++){
		double tmp = 2*qx[i]*DxList[i]*dx[i];
		dq[i] = 2*Fx[i]*DxList[i]*dx[i];
		d_alpha += tmp * log(x[i]);
		d_beta  += tmp * log(1-x[i]);
	}
	auto time_1 = chrono::system_clock::now();
	chrono::duration<double> time_diff = time_1 - time_0;
	return time_diff.count();
}

void findpdf::model_fit(int sample, int attemp, double learning_rate_start, const string& prior_dir){
	int max_patience_cnt = 30;		//maxium patience for continuous growth
	int max_patience_thm = 1000;	//maxium patience for thermal fluctuation

	time_t my_time = time(NULL);
	cout << time_now() <<"Training started." << endl;

	depth_DNN = 3;
	vector<int> width = {1, 16, 16, 1};
	vector<int> actfun = {2, 2, 5};
	string surfix = "44_";
	//0=tanh, 1=relu, 2=elu, 3=l-relu, 4=softplus, 5=linear
	for(int i=0; i<depth_DNN; i++){surfix = surfix+to_string(actfun[i]);}

	string prefix = "./pdfdnn_weight_and_bias_"+surfix;
	string prefix_parameters = "./pdfdnn_abn_"+surfix;
	string filein;
	string filein_parameter;
	
	double learning_rate = learning_rate_start;
	double regularizer = 0.001;

	if(attemp==0){
		//q_DNN.init(depth_DNN, width, NX);
		//q_DNN.set_activation_function(actfun);
		//q_DNN.parameter_init();
		filein = prefix+"-initial-imag.dat";
		filein_parameter = prefix_parameters+"-initial-imag.dat";
		q_DNN.import_model(NX, filein);
		import_parameters(filein_parameter);
	}else{
		filein = prefix+"-"+to_string(attemp)+".dat";
		filein_parameter = prefix_parameters+"-"+to_string(attemp)+".dat";
		q_DNN.import_model(NX, filein);
		import_parameters(filein_parameter);
	}
	q_DNN.assign_x(x);
	q_DNN.summary();
	q_DNN.set_optimizer_type(0);

    // import prior file from real part
    int iPP = 1;
    string filename_parameters_Samples = prior_dir + "/MeanErr/pdf_"+surfix+"_parameters-samples-iP"+to_string(iPP)+".dat";
    ifstream finP(filename_parameters_Samples);
    double tmp;
    finP >> tmp;
    priorWidth_beta = tmp/100;
    for (int isamp=-1; isamp<=sample; isamp++){
        finP >> tmp;
    }
    finP.close();
    prior_beta = tmp;
    cout << "+      Prior beta: " << prior_beta << ", width: " << priorWidth_beta << endl; 

	int N_epoch = 10000;
	int N_div = 1;/*10;
	if(attemp==2){N_div=5;}
	else if (attemp==3){N_div=2;}
	else if (attemp>3){N_div=1;}*/
	int batch_size = NX/N_div;
	double* dq_in = (double *) calloc (batch_size, sizeof(double));
	int * x_in = (int *) calloc (batch_size, sizeof(int)); 

	double chi_sq_last = 10000, chi_sq_min = 10000;
	int epoch_total = 1;


	// initialize ADAM
	q_DNN.optimizer_init();
	fac_m = 1;
	fac_s = 1;
	m_alpha = 0;
	s_alpha = 0;
	m_beta  = 0;
	s_beta  = 0;
	m_r = 0;
    s_r = 0;

	for(i_step=0; i_step<N_step; i_step++){
		cout << time_now() <<"Step ("<< i_step+1<<"/"<< N_step 
				<< "):\tlearning_rate = "<<learning_rate << endl;
		int patience_cnt = max_patience_cnt;
		int patience_thm = max_patience_thm;
		for(i_epoch=1; i_epoch<=N_epoch; i_epoch++){
			double dt_1 = q_DNN.forward();
			double dt_2 = compute_loss();
			for(int i=0; i<batch_size; i++){
				int k = i*N_div + (rand()%N_div);
				x_in[i] = k;
				dq_in[i] = dq[k];
			}
			double dt_3 = q_DNN.backward(batch_size, x_in, dq_in);
			q_DNN.update(learning_rate, regularizer);

			if ((epoch_total>10)&&(chi_sq > chi_sq_last)) {patience_thm-=1;patience_cnt-=1;}
			else{ patience_cnt = max_patience_cnt; }
			if (chi_sq_min>chi_sq){chi_sq_min=chi_sq;}
			if ((patience_cnt<0)||(patience_thm<0)){
				if (learning_rate>1E-5){learning_rate *= 0.3;}
                else {learning_rate = learning_rate_start;}
				// re-initialze ADAM
				q_DNN.optimizer_init();
				fac_m = 1;
				fac_s = 1;
				m_alpha = 0;
				s_alpha = 0;
				m_beta  = 0;
				s_beta  = 0;
				m_r = 0;
                s_r = 0;
                i_epoch = 0;
                dt_2 = compute_loss();
				break;
			}
			q_DNN.accept_update();
			//update alpha and beta according to ADAM
			fac_m *= beta_m;
			fac_s *= beta_s;
			m_alpha = beta_m * m_alpha + beta_mg * d_alpha;
			s_alpha = beta_s * s_alpha + beta_sg * d_alpha*d_alpha;
			alpha  -= learning_rate * (m_alpha/(1-fac_m)) / (sqrt(s_alpha/(1-fac_s) + epsilon)); 
			m_beta  = beta_m * m_beta  + beta_mg * d_beta;
			s_beta  = beta_s * s_beta  + beta_sg * d_beta*d_beta;
			beta   -= learning_rate * (m_beta/(1-fac_m))  / (sqrt(s_beta/(1-fac_s)  + epsilon));
			beta = prior_beta;
		    m_r = beta_m * m_r + beta_mg * d_r;
            s_r = beta_s * s_r + beta_sg * d_r*d_r;
            r  -= learning_rate * (m_r/(1-fac_m)) / (sqrt(s_r/(1-fac_s) + epsilon));
            r = 0;
            m_norm = beta_m * m_norm + beta_mg * d_norm;
            s_norm = beta_s * s_norm + beta_sg * d_norm*d_norm;
            norm  -= learning_rate * (m_norm/(1-fac_m)) / (sqrt(s_norm/(1-fac_s) + epsilon));

			epoch_total += 1;
			chi_sq_last = chi_sq;
			if (N_print==0) continue;
			if ((i_epoch%N_print!=0) and ((attemp>0) or (i_step>0) or (i_epoch>N_print)) ) continue;
			int exit = 0;
			for(int idx=1; idx < log10(N_print); idx++){
				int iprint=pow(10,idx);
				if ((i_epoch>3*iprint) && (i_epoch%iprint!=0)) exit=1;
			}
			if (exit==1) continue;
			cout << time_now() <<"Epoch ("<<setw(4)<< i_epoch<<"/"<< N_epoch 
				<< "):\tTimer=("<< setprecision(3)<<setw(3)<<int(1000*(dt_1))
				<<"+"<<setw(3)<<int(1000*dt_2)<<"+"<<setw(3)<<int(1000*(dt_3))
				<<")ms,\t[N,alpha,beta,r]=["<<norm<<","<<alpha<<","<<beta<<","<<r<<"]"
				<<"\tChi_Squared= "<< setprecision(4)<<setw(5)<<chi_sq<<" / "<<chi_sq_min
                << endl;
                //<<"\tlearning_rate = " << learning_rate
				//<< endl;

			#if (PRINT_LEVEL>1)
			if (i_step>0) continue;
			string filename_pdf_ij = "./pdf/pdf_"+surfix+"_sample"+to_string(sample)+"-"+to_string(attemp+1)+"-"+to_string(i_step+1)+"-"+to_string(i_epoch)+".dat";
			string filename_pdfQ_ij = "./pdf/pdf_"+surfix+"_sample"+to_string(sample)+"-"+to_string(attemp+1)+"-"+to_string(i_step+1)+"-"+to_string(i_epoch)+"-Q.dat";
            string filename_pdfITD_ij = "./pdf/pdf_"+surfix+"_sample"+to_string(sample)+"-"+to_string(attemp+1)+"-"+to_string(i_step+1)+"-"+to_string(i_epoch)+"-ITD.dat";
			export_pdf(filename_pdf_ij);
			#endif
		}
		#if (PRINT_LEVEL>0)
		string filename_pdf_i = "./pdf/pdf_"+surfix+"_sample"+to_string(sample)+"-"+to_string(attemp+1)+"-"+to_string(i_step+1)+".dat";
		string filename_pdfQ_i = "./pdf/pdf_"+surfix+"_sample"+to_string(sample)+"-"+to_string(attemp+1)+"-"+to_string(i_step+1)+"-Q.dat";
        string filename_parameters_i = "./param/pdf_"+surfix+"_sample"+to_string(sample)+"-"+to_string(attemp+1)+"-"+to_string(i_step+1)+"-param.dat";
        string filename_pdfITD_i = "./pdf/pdf_"+surfix+"_sample"+to_string(sample)+"-"+to_string(attemp+1)+"-"+to_string(i_step+1)+"-ITD.dat";
		export_pdf(filename_pdf_i);
        export_Q(filename_pdfQ_i);
        export_ITD(filename_pdfITD_i);
        export_parameters(filename_parameters_i);
		#endif
	}
	cout <<time_now()<<"Training completed!" << endl;
	string filename = prefix+"-"+to_string(attemp+1)+".dat";
	string filename_parameters = "./pdf/pdf_"+surfix+"_sample"+to_string(sample)+"-"+to_string(attemp+1)+"-parameters.dat";
    string filename_moments = "./pdf/pdf_"+surfix+"_sample"+to_string(sample)+"-"+to_string(attemp+1)+"-moments.dat";
	string filename_pdf = "./pdf/pdf_"+surfix+"_sample"+to_string(sample)+"-"+to_string(attemp+1)+".dat";
	string filename_pdfQ = "./pdf/pdf_"+surfix+"_sample"+to_string(sample)+"-"+to_string(attemp+1);
    string filename_pdfITD = "./pdf/pdf_"+surfix+"_sample"+to_string(sample)+"-"+to_string(attemp+1)+"-ITD.dat";
	q_DNN.export_weight_and_bias(filename);
	export_parameters(filename_parameters);
    export_moments(filename_moments);
	export_pdf(filename_pdf);
    export_Q(filename_pdfQ);
    export_ITD(filename_pdfITD);
	free(dq_in);
	free(x_in);
	return;
}

void findpdf::export_parameters(const string& filename){
	ofstream fout(filename);
	fout << alpha <<"\t#alpha"<<endl;
	fout << beta <<"\t#beta"<<endl;
	fout << r <<"\t#r"<<endl;
	fout << norm <<"\t#norm"<<endl;
	fout << chi_sq <<"\t#chisq"<<endl;
	fout.close();
}

void findpdf::import_parameters(const string& filename){
	ifstream fin(filename);
	string buff;
	fin >> alpha >> buff;
    cout << filename << " " << alpha << endl;
	assert(buff.compare("#alpha")==0);
	fin >> beta >> buff;
	assert(buff.compare("#beta")==0);
	fin >> r >> buff;
    assert(buff.compare("#r")==0);
	fin >> norm >> buff;
	assert(buff.compare("#norm")==0);
	fin.close();
}

#endif
