#include <math.h>
#include <random>
#include <chrono>
#include "Eigen/Dense"
#include <iostream>
#include <vector>
//#include "Arsenal.h"

using namespace std;


#ifndef MULTIDIMNORM
#define MULTIDIMNORM

class multi_normal_distribution{
public:
	multi_normal_distribution(){};
	void init(int dim_in);
	void set_covmat(double* CovMat_in);
	void set_invcovmat(double* InvCovMat_in);
	double sigma(int i){return StdDev[i];}
	void Free();
	double get_sample(double *x);
	void summary();
	void set_covmat_relevant(double* CovMat_in, int n_rlv);
	void set_invcovmat_relevant(double* InvCovMat_in, int n_rlv, double regularizer);
	double get_sample_relevant(double *x);
	void resign_stddev_relevant(double * StdDev_in);
	double eigen_vector(int i, int j){return EigenVector[i*dim+j];}
private:
	int dim;
	int dim_rlv;
	double *EigenVector;
	double *StdDev;
	normal_distribution<double> gauss;
	//random_device rd;
	mt19937 e2;//rd()
};

void multi_normal_distribution::init(int dim_in){
	double threshold = 0.001;
	dim = dim_in;
	StdDev = (double *) calloc(dim, sizeof(double));
	EigenVector = (double *) calloc(dim*dim, sizeof(double));
}

void multi_normal_distribution::set_covmat(double* CovMat_in){
	Eigen::MatrixXd cov_mat(dim,dim);
	for(int i=0; i<dim; i++) for(int j=0; j<dim; j++){
		cov_mat(i,j) = CovMat_in[i*dim+j];
	}
	
	Eigen::EigenSolver<Eigen::MatrixXd> eigensolver;
	
	eigensolver.compute(cov_mat);
	
	Eigen::VectorXd eigen_values = eigensolver.eigenvalues().real();
	Eigen::MatrixXd eigen_vectors = eigensolver.eigenvectors().real();

	for(int i=0; i<dim; i++){
		if (eigen_values[i]<0){
			StdDev[i] = 0;
			if (fabs(eigen_values[i])>1E-6){
				cerr<<"Warning! multi_normal_distribution::find small eigenvalue for inv_cov_mat: "<<eigen_values[i]<<endl;
			}
		}else{
			StdDev[i] = sqrt(eigen_values[i]);
		}
		for(int j=0; j<dim; j++){
			EigenVector[i*dim+j] = eigen_vectors(j,i);
		}
	}
}

void multi_normal_distribution::set_invcovmat(double* InvCovMat_in){
	double threshold = 1E-4;
	Eigen::MatrixXd inv_cov_mat(dim,dim);
	for(int i=0; i<dim; i++) for(int j=0; j<dim; j++){
		inv_cov_mat(i,j) = InvCovMat_in[i*dim+j];
	}
	
	Eigen::EigenSolver<Eigen::MatrixXd> eigensolver;
	
	eigensolver.compute(inv_cov_mat);
	
	Eigen::VectorXd eigen_values = eigensolver.eigenvalues().real();
	Eigen::MatrixXd eigen_vectors = eigensolver.eigenvectors().real();

	//for(int i=0; i<dim; i++){ cout << " " << eigen_values[i];}
	//cout << endl;
	for(int i=0; i<dim; i++){
		if (eigen_values[i]<threshold){
			StdDev[i] = 0;
			if (fabs(eigen_values[i])>1E-6){
				cerr<<"Warning! multi_normal_distribution::find small eigenvalue for inv_cov_mat: "<<eigen_values[i]<<endl;
			}
		}else{
			StdDev[i] = 1/sqrt(eigen_values[i]);
		}
		for(int j=0; j<dim; j++){
			EigenVector[i*dim+j] = eigen_vectors(j,i);
		}
	}
}

void multi_normal_distribution::summary(){
	cout <<"Dim = "<<dim<<endl;
	cout <<"StdDev = [";
	for(int i=0; i<dim; i++){
		cout << " " << StdDev[i];
	}
	cout <<" ],"<< endl;
	for(int i=0; i<dim; i++){
		for(int j=0; j<dim; j++){
			cout << "\t" << EigenVector[i*dim+j];
		}
		cout << endl;
	}
}

void multi_normal_distribution::Free(){
	free(EigenVector);
	free(StdDev);
}

double multi_normal_distribution::get_sample(double *x){
	for(int i=0; i<dim; i++){x[i] = 0;}
	double x_sq = 0;
	for(int i=0; i<dim; i++){
		double c0 = gauss(e2);
		double c_i = c0 * StdDev[i];
		x_sq += c0*c0;
		for(int j=0; j<dim; j++){x[j]+=c_i*EigenVector[i*dim+j];}
	}
	return x_sq;
}

void multi_normal_distribution::set_invcovmat_relevant(double* InvCovMat_in, int n_rlv, double regularizer){
	dim_rlv = 0;
	Eigen::MatrixXd inv_cov_mat_full(dim,dim);
	for(int i=0; i<dim; i++) for(int j=0; j<dim; j++){
		inv_cov_mat_full(i,j) = InvCovMat_in[i*dim+j];
	}
	vector<Eigen::VectorXd> vec_relevant;
	int n_power = 20;
	// I. Find the principal components
	for(int i=0; i<dim; i++){
		Eigen::VectorXd vec_loc(dim);
		// 1. initialize
		for(int j=0; j<dim; j++){
			vec_loc(j)=1;
			//if(j==i_int){vec_loc(j)=1;}
			//else{vec_loc(j)=0;}
		}
		// 2. suppress the null components (by acting the inv_cov_mat on it)
		for(int k=0; k<n_power; k++){
			// 2-1. Schmidt orthogonalization
			for(int j=0; j<i; j++){
				double fac = vec_relevant[j].transpose() * vec_loc;
				vec_loc = vec_loc - vec_relevant[j] * fac;
				vec_loc.normalize();
			}
			vec_loc = inv_cov_mat_full * vec_loc;
			vec_loc.normalize();
		}
		// 3. Schmidt orthogonalization
		for(int j=0; j<i; j++){
			double fac = vec_relevant[j].transpose() * vec_loc;
			vec_loc = vec_loc - vec_relevant[j] * fac;
			//cout <<"("<<i<<","<<j<<")\t"<<fac<<"\t"<< vec_relevant[j].transpose() * vec_loc << endl;
		}
		// 4. check linear dependency
		if(vec_loc.norm()<0.01){
			break;
			cerr << i<<"-th, norm = "<<vec_loc.norm() <<endl;
		}
		dim_rlv += 1;
		vec_loc.normalize();
		vec_relevant.push_back(vec_loc);
		//cout << vec_relevant[i].transpose() << endl;
	}
	// II. Get the equivalent small matrix
	Eigen::MatrixXd inv_cov_mat(dim_rlv,dim_rlv);
	for(int i=0; i<dim_rlv; i++){
		Eigen::VectorXd vec_loc = inv_cov_mat_full * vec_relevant[i];
		double norm = 0;
		for(int j=0; j<dim_rlv; j++){
			double coef = vec_loc.transpose() * vec_relevant[j];
			norm += coef*coef;
			inv_cov_mat(i,j) = coef;
			if ((j!=i)&&(fabs(coef)>1E-6)) {
				cerr<<"Warning:: Large non-diagonal at ("<<i<<","<<j<<"), = "<< coef<<endl;
			}
		}
		//cout << sqrt(norm) <<"\t"<< vec_loc.norm() << endl;
	}
	//cout << inv_cov_mat << endl;
	for(int i=0; i<dim; i++){StdDev[i] = 1/sqrt(regularizer);}
	for(int i=0; i<dim; i++)for(int j=0; j<dim; j++){EigenVector[i*dim+j]=0;}

	for(int i=0; i<dim_rlv; i++){StdDev[i] = 1/sqrt(regularizer+inv_cov_mat(i,i));}
	for(int i=0; i<dim_rlv; i++) for(int j=0; j<dim; j++){
		EigenVector[i*dim+j]=vec_relevant[i](j);
	}
	// III. Find the irrelevant directions
	int i_int = 0;
	for(int i=dim_rlv; i<dim; i++){
		Eigen::VectorXd vec_loc(dim);
		// 1. initialization
		for(int j=0; j<dim; j++){
			if(j==i_int){vec_loc(j)=1;}
			else{vec_loc(j)=0;}
		}
		// 2. Schmidt orthogonalization
		for(int j=0; j<i; j++){
			double fac = vec_relevant[j].transpose() * vec_loc;
			vec_loc = vec_loc - vec_relevant[j]*fac;
		}
		i_int += 1;
		// 3. check linear dependency
		if(vec_loc.norm()<0.01){i-=1;continue;}
		// 4. assign
		vec_loc.normalize();
		vec_relevant.push_back(vec_loc);
		for(int j=0; j<dim; j++){
			EigenVector[i*dim+j]=vec_relevant[i](j);
		}
		// 5. end if running out of vectors
		if((i_int==dim)&&(vec_relevant.size()<dim)){
			cerr<<"Only find "<<vec_relevant.size()<<" indept vectors"<<endl; 
			break;
		}
	}
	vec_relevant.clear();
	return;
}

double multi_normal_distribution::get_sample_relevant(double *x){
	for(int i=0; i<dim; i++){x[i] = 0;}
	double x_sq = 0;
	for(int i=0; i<dim_rlv; i++){
		double c0 = gauss(e2);
		double c_i = c0 * StdDev[i];
		x_sq += c0*c0;
		for(int j=0; j<dim; j++){x[j]+=c_i*EigenVector[i*dim+j];}
	}
	return x_sq;
}

void multi_normal_distribution::resign_stddev_relevant(double * variance_in){
	for(int i=0; i<dim_rlv; i++){StdDev[i]=sqrt(variance_in[i]);}
	return;
}

#endif