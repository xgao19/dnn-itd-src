#define N_print 1000
#define PRINT_LEVEL 0

#include <sstream>
#include <time.h>
#include <chrono>
#include <iomanip>
#include "FindPDF_REandIM.h"

int main(int argc, char **argv)
{
	string directory;
	string prefix;
	int Ns, Nt, Pmin, Pmax, Zmin, Zmax, sample;
	double dzfm;
	double dPGeV;

    const int P0 = 0;
    const int Plist[3] = {1,4,6};
    const int NP = 3;
    double delta = 0;	
	
	if (argc != 10) {
		cerr << "Usage: ./exec delta Ns Nt Zmin Zmax dz sample directory prefix"<<endl;
		return 1;
	}

    stringstream ss_delta, ss_Ns, ss_Nt, ss_Zmin, ss_Zmax, ss_dz, ss_sample; //ss_lambdaL, 
    ss_delta << argv[1];
    ss_delta >> delta;
	ss_Ns << argv[2];
	ss_Ns >> Ns;
	ss_Nt << argv[3];
	ss_Nt >> Nt;
	ss_Zmin << argv[4];
	ss_Zmin >> Zmin;
	ss_Zmax << argv[5];
	ss_Zmax >> Zmax;
	ss_dz << argv[6];
	ss_dz >> dzfm;
    //ss_lambdaL << argv[8];
    //ss_lambdaL >> lambdaL;
	ss_sample << argv[7];
	ss_sample >> sample;
	directory = string(argv[8]);
	prefix = string(argv[9]);
	dPGeV = 2*M_PI/(Ns*dzfm/hbarc); // GeV

	cout <<"=============================================================="<<endl;
	cout <<"+	directory:   " << directory << endl;
	cout <<"+	file_prefix: " << prefix << endl;
	cout <<"+	for sample : " << sample << endl;
	cout <<"+	Grid_Size of Lattice: " << Ns << "x" << Nt << endl;
    cout <<"+	Grid_P0 = " << P0 <<", Pmax = "<<Plist[NP-1]<<"]" << endl;
	cout <<"+	Grid_z  in [" << Zmin <<","<<Zmax<<"]" << endl;
	cout <<"+	dz in fm:     " << dzfm << endl;
	cout <<"+   dP in GeV:    " << dPGeV << endl;
    cout <<"+   DNN constrain delta: " << delta << endl;
	cout <<"=============================================================="<<endl;
	findpdf pdf_fit;
	pdf_fit.init();
    pdf_fit.load_lattice_data(directory, prefix, delta, sample, Ns, P0, NP, Plist, Zmin, Zmax, dzfm);
	pdf_fit.model_fit(sample,0,1E-3);
}
