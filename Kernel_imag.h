#include <fstream>
#include <iostream>
#include <string>
#include <cassert>
#include "Constants.h"
using namespace std;

#define ALPHA_ORDER 0
// 0 = LO, 1 = NLO, 2 = NNLO

#ifndef KERNEL__H__IM
#define KERNEL__H__IM

class kernel_imag{
public:
	kernel_imag(){};
	void init(const string & filename){
		c10_list = new double[Nlist];
		c20_list = new double[Nlist];
		c11_list = new double[Nlist];
		c21_list = new double[Nlist];
		c22_list = new double[Nlist];
		ifstream fin(filename);
		double x, c10_in, c20_in, c11_in, c21_in, c22_in;
		for(int i=0; i<Nlist; i++){
			fin >> x >> c10_in >> c20_in >> c11_in >> c21_in >> c22_in;
			assert(fabs(x*inv_dl - i)<1E-6);
			c10_list[i] = c10_in;
			c20_list[i] = c20_in;
			c11_list[i] = c11_in;
			c21_list[i] = c21_in;
			c22_list[i] = c22_in;
		}
		fin.close();
		string name;
		#if (ALPHA_ORDER == 0)
			name = "LO";
		#elif (ALPHA_ORDER == 1)
			name = "NLO";
		#elif (ALPHA_ORDER == 2)
			name = "NNLO";
		#endif
		cout << name <<" Kernal initialized!"<< endl;
	}
	void Free(){
		delete[] c10_list;
		delete[] c20_list;
		delete[] c11_list;
		delete[] c21_list;
		delete[] c22_list;
	}
	double C(const double & x, const double & z){
		double ix = fabs(x)*inv_dl;
		int i = floor(ix);
		if (i>Nlist-2){
			cerr<<"x out of range in kernel C..."<<endl;
			return 0;
		}
		ix -= i;
		double jx = 1-ix;
		double denom = 1;
		double numer = 0;
	#if (ALPHA_ORDER > 0)
		double a1 = alpha_s/2./M_PI;
		double L1 = 2*(gammaE + log(mu*z/2));
		double c10 = jx*c10_list[i] + ix*c10_list[i+1];
		double c11 = jx*c11_list[i] + ix*c11_list[i+1];
		denom += a1*(d10 + d11*L1);
		numer += a1*(c10 + c11*L1);
	#endif 
	#if (ALPHA_ORDER > 1)
		double a2 = a1*a1;
		double L2 = L1*L1;
		double c20 = jx*c20_list[i] + ix*c20_list[i+1];
		double c21 = jx*c21_list[i] + ix*c21_list[i+1];
		double c22 = jx*c22_list[i] + ix*c22_list[i+1];
		denom += a2*(d20 + d21*L1 + d22*L2);
		numer += a2*(c20 + c21*L1 + c22*L2);
	#endif 
		return -sin(x) + numer/denom;
	}
private:
	const int Nlist = 5001;
	const int inv_dl = 100; // dx = 1/100;
	double * c10_list;
	double * c20_list;
	double * c11_list;
	double * c21_list;
	double * c22_list;
};

#endif
