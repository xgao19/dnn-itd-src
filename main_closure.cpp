#define N_print 1000
#define PRINT_LEVEL 2

#include <sstream>
#include <time.h>
#include <chrono>
#include <iomanip>
#include "FindPDF.h"

int main(int argc, char **argv)
{
	findpdf pdf_fit;
	pdf_fit.init();
	pdf_fit.load_mock_data();
	pdf_fit.model_fit(0,1E-4);
}
