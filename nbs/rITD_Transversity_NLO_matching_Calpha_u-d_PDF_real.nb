(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     22947,        636]
NotebookOptionsPosition[     21775,        611]
NotebookOutlinePosition[     22189,        628]
CellTagsIndexPosition[     22146,        625]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 StyleBox[
  RowBox[{"Constants", ";"}], "Title"], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"CF", "=", 
   RowBox[{"4", "/", "3"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"nf", "=", "3"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"TF", "=", 
   RowBox[{"1", "/", "2"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"CA", "=", "3"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"alps0", "=", "0.296"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"fmGeV", "=", "5.0676896"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"mu0", "=", 
   RowBox[{"2", "*", "fmGeV"}]}], "  ", 
  RowBox[{"(*", 
   RowBox[{"fm", "^", 
    RowBox[{"-", "1"}]}], "*)"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gammaE", "=", "0.5772156649"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"beta0", "=", 
   FractionBox[
    RowBox[{
     RowBox[{"11", "CA"}], "-", 
     RowBox[{"4", "nf", " ", "TF"}]}], "6"]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"beta1", "=", 
   FractionBox[
    RowBox[{"102", "-", 
     RowBox[{"38", 
      RowBox[{"nf", "/", "3"}]}]}], "4"]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"alpsmuNLO", "[", "mu_", "]"}], ":=", 
   FractionBox["alps0", 
    RowBox[{"1", "+", 
     RowBox[{
      FractionBox["beta0", 
       RowBox[{"2", "\[Pi]"}]], "*", "alps0", "*", 
      RowBox[{"Log", "[", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"mu", "/", "mu0"}], ")"}], "^", "2"}], "]"}]}]}]]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"L", "[", 
    RowBox[{"mu_", ",", "z_"}], "]"}], ":=", 
   RowBox[{"Log", "[", 
    FractionBox[
     RowBox[{
      SuperscriptBox["mu", "2"], "*", 
      SuperscriptBox["z", "2"], "*", 
      SuperscriptBox["\[ExponentialE]", 
       RowBox[{"2", "*", "gammaE"}]]}], "4"], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"as", "[", "alps_", "]"}], ":=", 
    FractionBox["alps", 
     RowBox[{"2", " ", "\[Pi]"}]]}], ";"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
     RowBox[{"gmOf1", "[", 
      RowBox[{"alps_", ",", "mu_", ",", "z_"}], "]"}], "=", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"as", "[", "alps", "]"}], "2"], 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         SuperscriptBox["CF", "2"], 
         RowBox[{"(", 
          RowBox[{
           FractionBox[
            RowBox[{"-", "5"}], "8"], "+", 
           FractionBox[
            RowBox[{"2", 
             SuperscriptBox["\[Pi]", "2"]}], 
            RowBox[{"3", " "}]]}], ")"}]}], "+", 
        RowBox[{"CF", " ", "CA", " ", 
         RowBox[{"(", 
          RowBox[{
           FractionBox["49", "24"], "-", 
           FractionBox[
            SuperscriptBox["\[Pi]", "2"], "6"]}], ")"}]}], "+", 
        RowBox[{"CF", " ", "nf", " ", "TF", " ", 
         RowBox[{"(", 
          FractionBox[
           RowBox[{"\[Pi]", "-", "5"}], "6"], ")"}]}]}], ")"}], 
      RowBox[{"L", "[", 
       RowBox[{"mu", ",", "z"}], "]"}]}]}], ";"}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gmOf1", "=", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      SuperscriptBox["CF", "2"], 
      RowBox[{"(", 
       RowBox[{
        FractionBox[
         RowBox[{"-", "5"}], "8"], "+", 
        FractionBox[
         RowBox[{"2", 
          SuperscriptBox["\[Pi]", "2"]}], 
         RowBox[{"3", " "}]]}], ")"}]}], "+", 
     RowBox[{"CF", " ", "CA", " ", 
      RowBox[{"(", 
       RowBox[{
        FractionBox["49", "24"], "-", 
        FractionBox[
         SuperscriptBox["\[Pi]", "2"], "6"]}], ")"}]}], "+", 
     RowBox[{"CF", " ", "nf", " ", "TF", " ", 
      RowBox[{"(", 
       FractionBox[
        RowBox[{"-", "5"}], "6"], ")"}]}]}], ")"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"Hn", "[", "n_", "]"}], ":=", 
   RowBox[{"If", "[", 
    RowBox[{
     RowBox[{"n", "\[Equal]", "0"}], ",", "0", ",", 
     RowBox[{"Sum", "[", 
      RowBox[{
       RowBox[{"1", "/", "i"}], ",", 
       RowBox[{"{", 
        RowBox[{"i", ",", "1", ",", "n"}], "}"}]}], "]"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{" ", 
  RowBox[{
   RowBox[{
    RowBox[{"Hn2", "[", "n_", "]"}], ":=", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{"n", "\[Equal]", "0"}], ",", "0", ",", 
      RowBox[{"Sum", "[", 
       RowBox[{
        RowBox[{"1", "/", 
         SuperscriptBox["i", "2"]}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "1", ",", "n"}], "}"}]}], "]"}]}], "]"}]}], ";"}], 
  
  RowBox[{"(*", 
   RowBox[{"mu", " ", "is", " ", 
    RowBox[{"fm", "^", 
     RowBox[{"-", "1"}]}], " ", "here"}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.80812919504697*^9, 3.808129199579216*^9}, {
   3.80813583188264*^9, 3.808135861182249*^9}, 3.80813613997153*^9, {
   3.808175212631733*^9, 3.808175217046878*^9}, {3.808194982701027*^9, 
   3.808194989156435*^9}, {3.8081950346331177`*^9, 3.808195072505158*^9}, {
   3.808195200950577*^9, 3.808195207728973*^9}, {3.808195329689011*^9, 
   3.808195340115144*^9}, {3.808195371799595*^9, 3.808195509582239*^9}, {
   3.8081955774490347`*^9, 3.808195578648273*^9}, {3.808195642734993*^9, 
   3.808195655141485*^9}, {3.808195818503229*^9, 3.808195828278603*^9}, {
   3.808195905007081*^9, 3.8081959102270403`*^9}, {3.808195979527467*^9, 
   3.8081960788363533`*^9}, 3.808196138783825*^9, {3.8081961779581413`*^9, 
   3.8081961887880507`*^9}, {3.808196241545418*^9, 3.808196274468292*^9}, {
   3.808713277927928*^9, 3.8087133045808496`*^9}, 3.808898335879162*^9, {
   3.808953316230219*^9, 3.8089533165160646`*^9}, 3.808953362649757*^9, {
   3.808957957480276*^9, 3.8089580837972307`*^9}, {3.808958221135713*^9, 
   3.8089584686242523`*^9}, 3.809599767822019*^9, {3.809681174849419*^9, 
   3.809681194191181*^9}, {3.810435994675226*^9, 3.810435994912368*^9}, 
   3.8138916945250883`*^9, {3.81588423723662*^9, 3.815884242696437*^9}, {
   3.815886123090835*^9, 3.815886139258018*^9}, {3.8158861785963507`*^9, 
   3.81588620684764*^9}, {3.815891580290498*^9, 3.8158916125831957`*^9}, {
   3.81605133570378*^9, 3.816051344417397*^9}, {3.816298180665859*^9, 
   3.816298185663887*^9}, 3.816902105857594*^9, {3.816902277641181*^9, 
   3.8169023091697607`*^9}, {3.817279853822115*^9, 3.817279853938896*^9}, {
   3.8172799140245647`*^9, 3.817279914146439*^9}, {3.837467973056552*^9, 
   3.837467998923422*^9}, 3.837481002562511*^9},
 CellLabel->
  "In[658]:=",ExpressionUUID->"52636a0f-0534-4420-86dd-f048f47ab2ac"],

Cell[BoxData[{
 StyleBox[
  RowBox[{"Calpha0", ";"}], "Title"], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   StyleBox[
    RowBox[{"c1p1", "[", "a_", "]"}], "Text"], 
   StyleBox[":=", "Text"], 
   RowBox[{"DiracDelta", "[", 
    RowBox[{"1", "-", "a"}], "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.837478383020199*^9, 3.837478461996704*^9}, 
   3.83747875646601*^9, {3.837488618901449*^9, 3.837488630607315*^9}, 
   3.837495091051365*^9},
 CellLabel->
  "In[675]:=",ExpressionUUID->"e10c5a32-bc6b-41e1-ac81-d4cbc0b2793c"],

Cell[BoxData[{
 StyleBox[
  RowBox[{"Calpha1", ";"}], "Title"], "\[IndentingNewLine]", 
 StyleBox[
  RowBox[{"C1", ";"}], "Section"], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"c1p1", "[", 
     RowBox[{"a_", ",", "z_", ",", "mu_"}], "]"}], ":=", 
    RowBox[{"CF", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"2", 
        RowBox[{"L", "[", 
         RowBox[{"mu", ",", "z"}], "]"}]}], "+", "2"}], ")"}]}]}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"DiracDelta", "[", 
    RowBox[{"1", "-", "a"}], "]"}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"c1p2", "[", 
     RowBox[{"a_", ",", "z_", ",", "mu_"}], "]"}], ":=", 
    RowBox[{"CF", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         FractionBox[
          RowBox[{"2", "a"}], 
          RowBox[{"1", "-", "a"}]], ")"}], 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", 
           RowBox[{"L", "[", 
            RowBox[{"mu", ",", "z"}], "]"}]}], "-", "1"}], ")"}]}], "-", 
       RowBox[{"(", 
        FractionBox[
         RowBox[{"4", 
          RowBox[{"Log", "[", 
           RowBox[{"1", "-", "a"}], "]"}]}], 
         RowBox[{"1", "-", "a"}]], ")"}]}], ")"}]}]}], ";"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", "  ", 
   RowBox[{
    RowBox[{"+", "function"}], "  ", 
    RowBox[{"Theta", "[", "a", "]"}], "*", 
    RowBox[{"Theta", "[", 
     RowBox[{"1", "-", "a"}], "]"}]}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"c1p2L0", "[", 
    RowBox[{"a_", ",", "z_", ",", "mu_"}], "]"}], ":=", 
   RowBox[{"CF", "*", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"(", 
        FractionBox[
         RowBox[{"2", "a"}], 
         RowBox[{"1", "-", "a"}]], ")"}], 
       RowBox[{"(", 
        RowBox[{"-", "1"}], ")"}]}], "-", 
      RowBox[{"(", 
       FractionBox[
        RowBox[{"4", 
         RowBox[{"Log", "[", 
          RowBox[{"1", "-", "a"}], "]"}]}], 
        RowBox[{"1", "-", "a"}]], ")"}]}], ")"}], 
    RowBox[{"(", 
     RowBox[{"1", "-", "a"}], ")"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"c1p2L1", "[", 
    RowBox[{"a_", ",", "z_", ",", "mu_"}], "]"}], ":=", 
   RowBox[{"CF", "*", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"(", 
       FractionBox[
        RowBox[{"2", "a"}], 
        RowBox[{"1", "-", "a"}]], ")"}], 
      RowBox[{"(", 
       RowBox[{"-", "1"}], ")"}]}], ")"}], 
    RowBox[{"(", 
     RowBox[{"1", "-", "a"}], ")"}]}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.83747745553631*^9, 3.8374774632627773`*^9}, {
   3.8374783223767548`*^9, 3.8374783560122547`*^9}, {3.8374784699293213`*^9, 
   3.837478757393561*^9}, {3.837478838791507*^9, 3.837478841074131*^9}, {
   3.8374793479503736`*^9, 3.837479385218587*^9}, {3.8374794238349657`*^9, 
   3.837479438271497*^9}, {3.837480932261948*^9, 3.837480934289118*^9}, {
   3.837481077966826*^9, 3.837481078494635*^9}, {3.837481112087823*^9, 
   3.837481114722396*^9}, {3.8374815694626007`*^9, 3.8374815701888027`*^9}, {
   3.8374818248957987`*^9, 3.837481850637003*^9}, {3.8374886154869823`*^9, 
   3.837488616916768*^9}, 3.8374950897067842`*^9, {3.870897919989275*^9, 
   3.870897942644658*^9}, 3.870898006806467*^9, {3.87089838875229*^9, 
   3.870898393014309*^9}, {3.8929059969822807`*^9, 3.892905998474516*^9}, {
   3.892906127043151*^9, 3.89290618065289*^9}, {3.892906255386827*^9, 
   3.892906255606306*^9}, {3.892906287828336*^9, 3.892906316094076*^9}},
 CellLabel->
  "In[856]:=",ExpressionUUID->"a98ad08e-9d88-4ece-8c9e-ddde9cd0c74f"],

Cell[BoxData[{
 StyleBox[
  RowBox[{"CN1", ";"}], "Title"], "\[IndentingNewLine]", 
 RowBox[{"Clear", "[", 
  RowBox[{"mu", ",", "z", ",", "n"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"n", "=", "3"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"Integrate", "[", 
     RowBox[{
      RowBox[{
       FractionBox["1", 
        RowBox[{"2", " ", "\[Pi]"}]], 
       RowBox[{"c1p2", "[", 
        RowBox[{"a", ",", "z", ",", "mu"}], "]"}], 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"a", "^", "n"}], "-", "1"}], ")"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"a", ",", "0", ",", "1"}], "}"}]}], "]"}], 
    "\[IndentingNewLine]", 
    RowBox[{"Integrate", "[", 
     RowBox[{
      RowBox[{
       FractionBox["1", 
        RowBox[{"2", " ", "\[Pi]"}]], 
       RowBox[{"c1p2", "[", 
        RowBox[{
         RowBox[{"-", "a"}], ",", "z", ",", "mu"}], "]"}], 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"a", "^", "n"}], "+", "1"}], ")"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"a", ",", 
        RowBox[{"-", "1"}], ",", "0"}], "}"}]}], "]"}]}], "*)"}]}]}], "Input",\

 CellChangeTimes->{{3.892907008633074*^9, 3.892907018181045*^9}, {
   3.892907057916975*^9, 3.892907113256775*^9}, {3.8929071548931837`*^9, 
   3.892907190595613*^9}, {3.892907243378969*^9, 3.892907246215864*^9}, 
   3.892907361123352*^9, {3.892907486053228*^9, 3.89290748615495*^9}, {
   3.892907535780097*^9, 3.892907541331601*^9}, {3.8929075922963963`*^9, 
   3.892907669151083*^9}, {3.892907708167638*^9, 3.8929078118683157`*^9}, {
   3.892907871874576*^9, 3.892907872135079*^9}, {3.8929079575930653`*^9, 
   3.892907969365761*^9}, {3.892908009462377*^9, 3.892908037381546*^9}, 
   3.892908315355504*^9},
 CellLabel->
  "In[862]:=",ExpressionUUID->"56453af2-1a51-4777-b522-f24f0fcd2257"],

Cell[BoxData[{
 StyleBox[
  RowBox[{"CalphaNLO", ";"}], "Title"], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{
     StyleBox["cNLOp1", "Text"], "[", 
     RowBox[{"alpas_", ",", "a_", ",", "z_", ",", "mu_"}], "]"}], 
    StyleBox[":=", "Text"], 
    RowBox[{"1", "+", 
     RowBox[{
      FractionBox["alpas", 
       RowBox[{"2", " ", "\[Pi]"}]], 
      RowBox[{"c1p1", "[", 
       RowBox[{"a", ",", "z", ",", "mu"}], "]"}]}]}]}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"DiracDelta", "[", 
    RowBox[{"1", "-", "a"}], "]"}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{
     StyleBox["cNLOp2", "Text"], "[", 
     RowBox[{"alpas_", ",", "a_", ",", "z_", ",", "mu_"}], "]"}], 
    StyleBox[":=", "Text"], 
    RowBox[{
     FractionBox["alpas", 
      RowBox[{"2", " ", "\[Pi]"}]], 
     RowBox[{"c1p2", "[", 
      RowBox[{"a", ",", "z", ",", "mu"}], "]"}]}]}], ";"}], 
  RowBox[{"(*", "  ", 
   RowBox[{
    RowBox[{"+", "function"}], "  ", 
    RowBox[{"Theta", "[", "a", "]"}], "*", 
    RowBox[{"Theta", "[", 
     RowBox[{"1", "-", "a"}], "]"}]}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.837495106551231*^9, 3.837495223263063*^9}, 
   3.837495454695973*^9, {3.892904731849676*^9, 3.8929047430503263`*^9}},
 CellLabel->
  "In[865]:=",ExpressionUUID->"5bcbf7e8-e01e-4fb3-8027-891199bb18d8"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
   RowBox[{"qQ", "[", 
    RowBox[{"Pz_", ",", "z_", ",", "mu_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{
     RowBox[{
      StyleBox["cNLOp1", "Text"], "[", 
      RowBox[{"0.296", ",", "1", ",", "z", ",", "mu"}], "]"}], "*", 
     RowBox[{"Qitp", "[", 
      RowBox[{"z", "*", "Pz"}], "]"}]}], "+", 
    RowBox[{"NIntegrate", "[", 
     RowBox[{
      RowBox[{
       RowBox[{
        StyleBox["cNLOp2", "Text"], "[", 
        RowBox[{"0.296", ",", "a", ",", "z", ",", "mu"}], "]"}], " ", "*", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"Qitp", "[", 
          RowBox[{"a", "*", "z", "*", "Pz"}], "]"}], "-", 
         RowBox[{"Qitp", "[", 
          RowBox[{"z", "*", "Pz"}], "]"}]}], ")"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"a", ",", "0", ",", "1"}], "}"}], ",", 
      RowBox[{"Method", "\[Rule]", "\"\<LocalAdaptive\>\""}]}], "]"}]}]}], 
  "*)"}]], "Input",
 CellChangeTimes->{{3.8374947857913103`*^9, 3.837494789345536*^9}, {
   3.837494879713297*^9, 3.8374949044346437`*^9}, {3.837494956482027*^9, 
   3.8374949800431843`*^9}, {3.837495060667012*^9, 3.837495062151976*^9}, {
   3.837495536794402*^9, 3.837495542183017*^9}, {3.837495583776061*^9, 
   3.8374956975403423`*^9}, {3.8374957349195538`*^9, 3.83749577260145*^9}, {
   3.837495824924095*^9, 3.8374958315315533`*^9}, 3.837495973585834*^9, {
   3.837496279495891*^9, 3.8374962882563543`*^9}, {3.8374963248164062`*^9, 
   3.837496383428925*^9}, {3.8929047352008944`*^9, 3.892904735833305*^9}, 
   3.892906377860038*^9},
 CellLabel->
  "In[707]:=",ExpressionUUID->"34c32b22-0a52-44d4-8ad0-b054ac658de3"],

Cell[BoxData[{
 StyleBox[
  RowBox[{
   RowBox[{"CalphaNLO", " ", "DNN", " ", "kernels"}], ";"}], 
  "Title"], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"c1p2L0", "[", "a_", "]"}], ":=", 
   RowBox[{"CF", "*", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"(", 
        FractionBox[
         RowBox[{"2", "a"}], 
         RowBox[{"1", "-", "a"}]], ")"}], 
       RowBox[{"(", 
        RowBox[{"-", "1"}], ")"}]}], "-", 
      RowBox[{"(", 
       FractionBox[
        RowBox[{"4", 
         RowBox[{"Log", "[", 
          RowBox[{"1", "-", "a"}], "]"}]}], 
        RowBox[{"1", "-", "a"}]], ")"}]}], ")"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"c1p2L1", "[", "a_", "]"}], ":=", 
   RowBox[{"CF", "*", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"(", 
       FractionBox[
        RowBox[{"2", "a"}], 
        RowBox[{"1", "-", "a"}]], ")"}], 
      RowBox[{"(", 
       RowBox[{"-", "1"}], ")"}]}], ")"}]}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.870941668352412*^9, 3.8709416733029947`*^9}, {
  3.870941828795697*^9, 3.8709418372548847`*^9}, {3.872607682099276*^9, 
  3.872607689994482*^9}, {3.89290470266063*^9, 3.892904709033907*^9}, {
  3.892906405256372*^9, 3.892906409736684*^9}, {3.892908269165166*^9, 
  3.8929082867495947`*^9}},
 CellLabel->
  "In[868]:=",ExpressionUUID->"fb5a6b5b-f008-4d92-882b-a50514a78989"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
  "SaveFolder", "=", 
   "\"\</Users/Xiang/Desktop/docs/0-2023/projects/4-proton-transversity/1-DNN-\
PDF/DNN_proton_PDF/u-d/srcDNN/data\>\""}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"KernelSave", "=", 
   RowBox[{"{", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "0"}], "}"}], 
    "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"For", "[", 
  RowBox[{
   RowBox[{"i", "=", "1"}], ",", 
   RowBox[{"i", "\[LessEqual]", "5000"}], ",", 
   RowBox[{"i", "++"}], ",", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"x", "=", 
     RowBox[{"i", "/", "100."}]}], ";", "\[IndentingNewLine]", 
    RowBox[{"KernelSaveRow", "=", 
     RowBox[{"{", 
      RowBox[{"x", ",", 
       RowBox[{"NIntegrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"Cos", "[", 
             RowBox[{"a", "*", "x"}], "]"}], "-", 
            RowBox[{"Cos", "[", "x", "]"}]}], ")"}], 
          RowBox[{"c1p2L0", "[", "a", "]"}]}], ",", 
         RowBox[{"{", 
          RowBox[{"a", ",", "0", ",", "1"}], "}"}], ",", 
         RowBox[{"Method", "\[Rule]", "\"\<LocalAdaptive\>\""}]}], "]"}], ",", 
       RowBox[{"NIntegrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"Cos", "[", 
             RowBox[{"a", "*", "x"}], "]"}], "-", 
            RowBox[{"Cos", "[", "x", "]"}]}], ")"}], "0"}], ",", 
         RowBox[{"{", 
          RowBox[{"a", ",", "0", ",", "1"}], "}"}], ",", 
         RowBox[{"Method", "\[Rule]", "\"\<LocalAdaptive\>\""}]}], "]"}], ",", 
       RowBox[{"NIntegrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"Cos", "[", 
             RowBox[{"a", "*", "x"}], "]"}], "-", 
            RowBox[{"Cos", "[", "x", "]"}]}], ")"}], 
          RowBox[{"c1p2L1", "[", "a", "]"}]}], ",", 
         RowBox[{"{", 
          RowBox[{"a", ",", "0", ",", "1"}], "}"}], ",", 
         RowBox[{"Method", "\[Rule]", "\"\<LocalAdaptive\>\""}]}], "]"}], ",", 
       RowBox[{"NIntegrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"Cos", "[", 
             RowBox[{"a", "*", "x"}], "]"}], "-", 
            RowBox[{"Cos", "[", "x", "]"}]}], ")"}], "0"}], ",", 
         RowBox[{"{", 
          RowBox[{"a", ",", "0", ",", "1"}], "}"}], ",", 
         RowBox[{"Method", "\[Rule]", "\"\<LocalAdaptive\>\""}]}], "]"}], ",", 
       RowBox[{"NIntegrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"Cos", "[", 
             RowBox[{"a", "*", "x"}], "]"}], "-", 
            RowBox[{"Cos", "[", "x", "]"}]}], ")"}], "0"}], ",", 
         RowBox[{"{", 
          RowBox[{"a", ",", "0", ",", "1"}], "}"}], ",", 
         RowBox[{"Method", "\[Rule]", "\"\<LocalAdaptive\>\""}]}], "]"}]}], 
      "}"}]}], ";", "\[IndentingNewLine]", 
    RowBox[{"KernelSave", "=", 
     RowBox[{"AppendTo", "[", 
      RowBox[{"KernelSave", ",", "KernelSaveRow"}], "]"}]}], ";"}]}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"Print", "[", "KernelSaveRow", "]"}], ";"}], "*)"}], 
  "\[IndentingNewLine]", "]"}], "\[IndentingNewLine]", 
 RowBox[{"Export", "[", 
  RowBox[{
   RowBox[{"SaveFolder", "<>", "\"\</CbarList_NNLO_real.dat\>\""}], ",", 
   "KernelSave", ",", "\"\<Table\>\""}], "]"}]}], "Input",
 CellChangeTimes->{{3.872663317361989*^9, 3.872663488809388*^9}, {
   3.8726639459846697`*^9, 3.872663948210339*^9}, {3.8726640921032057`*^9, 
   3.872664100941784*^9}, {3.8726647747573557`*^9, 3.872664830088232*^9}, {
   3.872664873255786*^9, 3.872664877467643*^9}, 3.892904686011428*^9, 
   3.892904802437757*^9, {3.892908350006104*^9, 3.892908352490189*^9}, {
   3.892908404053046*^9, 3.8929084278377132`*^9}},
 CellLabel->
  "In[871]:=",ExpressionUUID->"c3558ef2-6f5c-4f24-bd51-c210a6435c02"],

Cell[BoxData["\<\"/Users/Xiang/Desktop/docs/0-2023/projects/4-proton-\
transversity/1-DNN-PDF/DNN_proton_PDF/u-d/srcDNN/data/CbarList_NNLO_real.dat\"\
\>"], "Output",
 CellChangeTimes->{3.892908526372135*^9},
 CellLabel->
  "Out[874]=",ExpressionUUID->"70576e44-fe87-438e-8006-4acb76ccc6f2"]
}, Open  ]]
},
Evaluator->"Local",
WindowSize->{1225, 824},
WindowMargins->{{144, Automatic}, {Automatic, 0}},
FrontEndVersion->"13.1 for Mac OS X x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"4bd9ee04-f1c7-4d2a-a045-efe46e68be33"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 6551, 181, 600, "Input",ExpressionUUID->"52636a0f-0534-4420-86dd-f048f47ab2ac"],
Cell[7112, 203, 530, 14, 80, "Input",ExpressionUUID->"e10c5a32-bc6b-41e1-ac81-d4cbc0b2793c"],
Cell[7645, 219, 3587, 101, 254, "Input",ExpressionUUID->"a98ad08e-9d88-4ece-8c9e-ddde9cd0c74f"],
Cell[11235, 322, 1842, 48, 157, "Input",ExpressionUUID->"56453af2-1a51-4777-b522-f24f0fcd2257"],
Cell[13080, 372, 1356, 40, 167, "Input",ExpressionUUID->"5bcbf7e8-e01e-4fb3-8027-891199bb18d8"],
Cell[14439, 414, 1630, 38, 52, "Input",ExpressionUUID->"34c32b22-0a52-44d4-8ad0-b054ac658de3"],
Cell[16072, 454, 1389, 43, 141, "Input",ExpressionUUID->"fb5a6b5b-f008-4d92-882b-a50514a78989"],
Cell[CellGroupData[{
Cell[17486, 501, 3979, 100, 262, "Input",ExpressionUUID->"c3558ef2-6f5c-4f24-bd51-c210a6435c02"],
Cell[21468, 603, 291, 5, 82, "Output",ExpressionUUID->"70576e44-fe87-438e-8006-4acb76ccc6f2"]
}, Open  ]]
}
]
*)

