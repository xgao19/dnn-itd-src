using namespace std;

#ifndef CONSTANT
#define CONSTANT

double hbarc = 0.197;
double mu = 2; // 2GeV.
double alpha_s = 0.296;
double CF = 4./3.;
double gammaE = 0.5772156649;

double d10 = 2.0*CF;
double d11 = 2.0*CF;
double d20 = 0.0;
double d21 = 0.0;
double d22 = 0.0;

#endif
